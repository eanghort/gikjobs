//
//  VLRootViewController.h
//  gikjobs
//
//  Created by Vanara Leng on 7/3/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GJFeedViewController.h"
#import "GJGeneralSettingViewController.h"
#import "GJPersonalViewController.h"
#import "Constants.h"
@interface GJRootViewController : UIViewController <UIPageViewControllerDataSource,UIPageViewControllerDelegate>
@property(strong,nonatomic) UIPageViewController *pageViewController;
//three page view
@property(strong,nonatomic) GJFeedViewController *recommendationViewController;
@property(strong,nonatomic) GJFeedViewController *feedViewController;
@property(strong,nonatomic) GJPersonalViewController *personalViewController;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButtonItem;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftButtonItem;
@property (strong, nonatomic) IBOutlet UILabel *pageTitle;

@end