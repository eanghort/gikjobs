//
//  VLRangeSlider.m
//  gikjobs
//
//  Created by Vanara Leng on 7/23/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJRangeSlider.h"
#import "Constants.h"
#import "GJImages.h"

@interface GJRangeSlider ()
-(float)xForValue:(float)value;
@property(strong,nonatomic) UILabel *minLabel;
@property(strong,nonatomic) UILabel *maxLabel;
@end

@implementation GJRangeSlider
@synthesize minimumValue, maximumValue, minimumRange, selectedMinimumValue, selectedMaximumValue;

- (id)initWithFrame:(CGRect)frame
           minValue:(float)paramMinValue
           maxValue:(float) paramMaxValue
           minRange:(float) paramMinRange
   selectedMinValue:(NSInteger)paramSelectedMinValue
   selectedMaxValue:(NSInteger)paramSelectedMaxValue

{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.minimumValue = paramMinValue;
        self.maximumValue = paramMaxValue;
        self.minimumRange = paramMinRange;
        self.selectedMinimumValue = paramSelectedMinValue;
        self.selectedMaximumValue = paramSelectedMaxValue;
        
        _minThumbOn = false;
        _maxThumbOn = false;
        _padding = 20;
        
        _trackBackground = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x+20, 0, self.frame.size.width-40, 2)];
        _trackBackground.backgroundColor = THEME_LIGHT_COLOR;

        _trackBackground.center = CGPointMake(frame.size.width/2, frame.size.height/2);
        _trackBackground.frame = CGRectMake((frame.size.width - _trackBackground.frame.size.width) / 2,_trackBackground.frame.origin.y, _trackBackground.frame.size.width, _trackBackground.frame.size.height);
        [self addSubview:_trackBackground];
    
        _track = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, 0, self.frame.size.width, 2)];
        _track.backgroundColor = THEME_COLOR;
        
        _track.center = CGPointMake(frame.size.width/2, frame.size.height/2);
        _track.frame = CGRectMake((frame.size.width - _track.frame.size.width) / 2,_track.frame.origin.y, _track.frame.size.width, _track.frame.size.height);
        [self addSubview:_track];
        
//        _minThumb = [[UIImageView alloc] initWithImage:[GJImages image:[UIImage imageNamed:@"slider-handle"] withColor:THEME_COLOR]];
        
        UIView *min = [[UIView alloc] initWithFrame:CGRectMake(0,_track.center.y-11, 22, 22)];
        min.backgroundColor = [UIColor whiteColor];
        min.layer.cornerRadius = 11.0;
        min.layer.borderWidth = 2.0;
        min.layer.borderColor = THEME_COLOR.CGColor;
        min.userInteractionEnabled = NO;
        
        _minThumb = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        [_minThumb addSubview:min];
        
        _minThumb.center = CGPointMake([self xForValue:selectedMinimumValue],_track.center.y);
        _minThumb.frame = CGRectMake(_minThumb.frame.origin.x, 0, _minThumb.frame.size.width, frame.size.height);
        [_minThumb setContentMode:UIViewContentModeCenter];
        
        [self addSubview:_minThumb];
        
        
        UIView *max = [[UIView alloc] initWithFrame:CGRectMake(0,_track.center.y-11, 22, 22)];
        max.backgroundColor = [UIColor whiteColor];
        max.layer.cornerRadius = 11.0;
        max.layer.borderWidth = 2.0;
        max.layer.borderColor = THEME_COLOR.CGColor;
        max.userInteractionEnabled = NO;
        
        _maxThumb = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        [_maxThumb addSubview:max];
        
        _maxThumb.center = CGPointMake([self xForValue:selectedMaximumValue],  frame.size.height/2);
        _maxThumb.frame = CGRectMake(_maxThumb.frame.origin.x,0, _maxThumb.frame.size.width, frame.size.height);
        [_maxThumb setContentMode:UIViewContentModeCenter];
        
        [self addSubview:_maxThumb];
        
        [self updateTrackHighglight];
        
        self.minLabel = [[UILabel alloc] init];
        [self.minLabel setText:[NSString stringWithFormat:@"%ld",(long)selectedMinimumValue]];
        self.minLabel.frame = CGRectMake(_minThumb.center.x-[self.minLabel sizeThatFits:self.frame.size].width/2, 0, [self.minLabel sizeThatFits:self.frame.size].width, 21);
        [self.minLabel setFont:[UIFont systemFontOfSize:15]];
        [self.minLabel setTextColor:THEME_COLOR];
        [self addSubview:self.minLabel];
        
        
        self.maxLabel = [[UILabel alloc] init];
        [self.maxLabel setText:[NSString stringWithFormat:@"%ld",(long)selectedMaximumValue]];
        self.maxLabel.frame = CGRectMake(_maxThumb.center.x-[self.maxLabel sizeThatFits:self.frame.size].width/2, 0, [self.maxLabel sizeThatFits:self.frame.size].width, 21);
        
        [self.maxLabel setFont:[UIFont systemFontOfSize:15]];
        [self.maxLabel setTextColor:THEME_COLOR];
        [self addSubview:self.maxLabel];
        
    }
    return self;
}
- (void)updateTrackHighglight{
    _track.frame = CGRectMake(_minThumb.center.x, _track.center.y- (_track.frame.size.height/2), _maxThumb.center.x - _minThumb.center.x, _track.frame.size.height);
    if (_minThumbOn) {
        [self.minLabel setText:[NSString stringWithFormat:@"%ld",(long)selectedMinimumValue]];
        self.minLabel.center = _minThumb.center;
        self.minLabel.frame = CGRectMake(_minThumb.center.x-[self.minLabel sizeThatFits:self.frame.size].width/2, 0, [self.minLabel sizeThatFits:self.frame.size].width, 21);
    }
    if (_maxThumbOn) {
        [self.maxLabel setText:[NSString stringWithFormat:@"%ld",(long)selectedMaximumValue]];
        self.maxLabel.center = _maxThumb.center;
        self.maxLabel.frame = CGRectMake(_maxThumb.center.x-[self.maxLabel sizeThatFits:self.frame.size].width/2, 0, [self.maxLabel sizeThatFits:self.frame.size].width, 21);
    }
    
}
- (float)xForValue:(float)value{
    return (self.frame.size.width-(_padding*2))*((value - minimumValue) / (maximumValue - minimumValue))+_padding;
}
-(float)valueForX:(float)x{
    int tempX = (minimumValue + (x-_padding) / (self.frame.size.width-(_padding*2)) * (maximumValue - minimumValue));
    //get value which can divide by 25
    return tempX -(tempX % 25);
}

-(BOOL) beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    CGPoint touchPoint = [touch locationInView:self];
    
    if(CGRectContainsPoint(_minThumb.frame, touchPoint)){
        _minThumbOn = true;
        [self.minLabel setTextColor:THEME_LIGHT_COLOR];
    }else if(CGRectContainsPoint(_maxThumb.frame, touchPoint)){
        _maxThumbOn = true;
        [self.maxLabel setTextColor:THEME_LIGHT_COLOR];
    }
    return YES;
}

-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    _minThumbOn = false;
    _maxThumbOn = false;
    [self.minLabel setTextColor:THEME_COLOR];
    [self.maxLabel setTextColor:THEME_COLOR];
}

-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    if(!_minThumbOn && !_maxThumbOn){
        return YES;
    }

    CGPoint touchPoint = [touch locationInView:self];
    if(_minThumbOn){
        _minThumb.center = CGPointMake(MAX([self xForValue:minimumValue],MIN(touchPoint.x, [self xForValue:selectedMaximumValue - minimumRange])), _minThumb.center.y);

        selectedMinimumValue = [self valueForX:_minThumb.center.x];
            [self.minLabel setTextColor:THEME_LIGHT_COLOR];
    }
    if(_maxThumbOn){
        _maxThumb.center = CGPointMake(MIN([self xForValue:maximumValue], MAX(touchPoint.x, [self xForValue:selectedMinimumValue + minimumRange])), _maxThumb.center.y);
        
        //for max
        [self.maxLabel setTextColor:THEME_LIGHT_COLOR];
        selectedMaximumValue = [self valueForX:_maxThumb.center.x];
    }
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    [self setNeedsDisplay];
    [self updateTrackHighglight];
    return YES;
}
- (void) setEnabled:(BOOL)enabled{
    [self setUserInteractionEnabled:enabled];
}

@end
