//
//  GJPickerViewController.m
//  gikjobs
//
//  Created by Vanara's Work on 9/11/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJPickerViewController.h"
#import "GJProfileSettingViewController.h"
#import "GJGeneralSettingViewController.h"
@interface GJPickerViewController () <UIPickerViewDataSource,UIPickerViewDelegate>
@property(nonatomic,strong) AFHTTPRequestOperationManager *manager;
@property (nonatomic,strong) NSMutableArray *list;


@end



@implementation GJPickerViewController


+(GJPickerViewController *)sharePickerView{
    static GJPickerViewController *sharePickerView;
    
    static dispatch_once_t onceToken;
    if (!sharePickerView) {
        dispatch_once(&onceToken, ^{
            
            sharePickerView = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"PickerViewController"];
            // modal present
            [sharePickerView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [sharePickerView setModalInPopover:YES];
            [sharePickerView setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        });
    }
    return sharePickerView;
}

- (void) loadListFromServerWithURL:(NSString*)urlString{
    [self.manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary  *responseObject){
        
        //on success
        self.list =responseObject[self.key];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.PickerView reloadAllComponents];
            [self.PickerView setHidden:NO];
            
            //get select
            self.selectTitle =  self.list[[self.PickerView selectedRowInComponent:0]][@"title"];
        });
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
    }];

}

#pragma mark - Picker Delegate and Datasource
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.list[row][@"title"];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.list count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.selectID = [self.list[row][@"id"] integerValue];
    self.selectTitle = self.list[row][@"title"];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.PickerView.delegate = self;
    self.PickerView.dataSource = self;
    
    self.manager = [AFHTTPRequestOperationManager manager];
    
    self.whiteView.layer.cornerRadius = 6;
    self.whiteView.layer.shadowOffset = CGSizeMake(0, 0);
    self.whiteView.layer.shadowOpacity = 0.1;

    
    UITapGestureRecognizer *singleFingerTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];

}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.PickerView setHidden:YES];
    
    if(self.selector)
    {
        [self.doneButton removeTarget:[GJProfileSettingViewController shareProfileSetting] action:self.selector forControlEvents:UIControlEventTouchUpInside];
    }
    [self.doneButton addTarget:[GJProfileSettingViewController shareProfileSetting] action: self.selector forControlEvents:UIControlEventTouchUpInside];
    [self loadListFromServerWithURL:self.url];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
