//
//  VLAppDelegate.h
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJPersonalViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *ProfileImageView;
@property (strong, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *PositionLabel;
@property (strong, nonatomic) IBOutlet UITableView *profileCategoryTable;

@end
