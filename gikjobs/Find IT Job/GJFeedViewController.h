//
//  VLAppDelegate.h
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>

@interface GJFeedViewController :UITableViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property BOOL isRecommendation;

@end
