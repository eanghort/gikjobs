//
//  VLLogInViewController.m
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJLogInViewController.h"
#import "GJAppDelegate.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#include "GJWebConnection.h"
#import "Constants.h"

@interface GJLogInViewController () <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *profilePictureView;
@property (strong, nonatomic) IBOutlet UIButton *dismissButton;
@property (strong, nonatomic) IBOutlet UILabel *username;
@end

@implementation GJLogInViewController
@synthesize ggSignInView, fbLoginView;


//singleton pattern for login view;

+ (GJLogInViewController *)shareLoginView{
    static GJLogInViewController *shareLoginView;

    static dispatch_once_t onceToken;
    if (!shareLoginView) {
        dispatch_once(&onceToken, ^{

            shareLoginView = [[UIStoryboard storyboardWithName:IPHONE_STORYBOARD_ID bundle:nil] instantiateViewControllerWithIdentifier:LOGIN_VIEW_CONTROLLER_ID];
        });
    }
    return shareLoginView;
}


//associate with storyboard
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self==[super initWithCoder:aDecoder]) {
        [self initLoginInstance];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setGradientBackground];
    [self initLoginButton];
 
    self.profilePictureView.layer.cornerRadius = 40;
}
-(void) setGradientBackground{
    // Create the colors
    UIColor *darkOp = UIColorFromRGB(0xF7F7F7);
    UIColor *lightOp = UIColorFromRGB(0xD7D7D7);
    // Create the gradient
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // Set colors
    gradient.colors = [NSArray arrayWithObjects:
                       (id)darkOp.CGColor,
                       (id)lightOp.CGColor,
                       nil];
    
    // Set bounds
    gradient.frame = self.view.bounds;
    
    // Add the gradient to the view
    [self.view.layer insertSublayer:gradient atIndex:0];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self resetLoginView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self resetLoginView];
}

- (void)didReceiveMemoryWarning{[super didReceiveMemoryWarning];}

- (void) resetLoginView{
            self.dismissButton.hidden = YES;
    if ([[GPPSignIn sharedInstance] authentication] || [[FBSession activeSession] isOpen]) {
//        NSLog(@"-- you're signed in");
        self.dismissButton.hidden = NO;
        if ([[GPPSignIn sharedInstance] authentication]) {
            [self toggleShowGoogleButton:YES];
            [self toggleShowFacebookButton:NO];
            
        }
        if ([[FBSession activeSession] isOpen]) {
            [self toggleShowFacebookButton:YES];
            [self toggleShowGoogleButton:NO];
        }
    }else{
//        NSLog(@"-- you're signed out");
        [self toggleShowFacebookButton:YES];
        [self toggleShowGoogleButton:YES];
    }
}

- (IBAction)ggSignout:(id)sender {
    [[GPPSignIn sharedInstance] signOut];
    [self resetLoginView];
}

-(void) toggleGoogleSigninButton{
    if([[GPPSignIn sharedInstance] authentication]){
        self.ggSignOutView.hidden = NO;
        self.ggSignInView.hidden = YES;

    }else{
        self.ggSignInView.hidden = NO;
        self.ggSignOutView.hidden = YES;
    }
}

- (void) toggleShowGoogleButton:(BOOL)show{
    if (show ==YES) {
        [self toggleGoogleSigninButton];
    }else{
        self.ggSignInView.hidden = YES;
        self.ggSignOutView.hidden = YES;
    }
}

- (void) toggleShowFacebookButton:(BOOL) show{
    if (show == YES) {
        self.fbLoginView.hidden = NO;
        self.profilePictureView.hidden = NO;
    }else{
        self.fbLoginView.hidden = YES;
        self.profilePictureView.hidden = YES;
    }
}
-(void) initLoginButton{
    //design facebook and google to be call in view did load
    
    CGFloat width = 280.0;
    CGFloat height = 50.0;
    CGFloat x = self.view.center.x-(width/2);
    CGFloat y = self.view.center.y-(height/2);
    self.fbLoginView.frame = CGRectMake(x, y, width, height);
    [self.view addSubview:self.fbLoginView];
    
    self.ggSignInView = [[GPPSignInButton alloc] initWithFrame:CGRectMake(x, y+56, width, height)];
    self.ggSignInView.style = kGPPSignInButtonStyleWide;
    [self.view addSubview:self.ggSignInView];
}

-(void) initLoginInstance{

    //init instance of facebook and google button
    
//facebook
    self.fbLoginView = [[FBLoginView alloc] initWithReadPermissions:@[@"public_profile",@"email",@"user_friends"]];
    self.fbLoginView.delegate = self;

//google
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.clientID = GOOGLE_CLIENT_ID;
    signIn.scopes = @[@"profile"];
    signIn.delegate = self;


//signin when app load
    [signIn trySilentAuthentication];
}

/* Google Signin delegate */

-(void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Google" message:@"Sign in error. Please sign in again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        if ([[GPPSignIn sharedInstance] authentication]) {
            [self dismissView]; //dismiss log in view after log in successful.
            [self resetLoginView];
        }
    }
}

/* Facebook Login delegate */
-(void) loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
	
	NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
	NSString* profile_id = [NSString stringWithFormat:@"fb%@", (NSString*) user.objectID];
	[dictionary setValue:profile_id forKey:@"profile_id"];
	[dictionary setValue:user.name forKey:@"name"];
	[dictionary setValue:[user objectForKey:@"email"] forKey:@"email"];
	[GJWebConnection sendDataViaUrl:@"gikjob" WithData:dictionary];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
	NSString *documentsPath = [paths objectAtIndex:0];
	
	UIImage* image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",(NSString*)user.objectID]]]];
	NSData* data = [NSData dataWithData:UIImagePNGRepresentation(image)];
	[data writeToFile:[documentsPath stringByAppendingPathComponent:@"image.png"] atomically:YES];

	[self.profilePictureView setImage:[UIImage imageWithData:data]];
	self.username.text = user.name;
}
-(void) loginViewShowingLoggedInUser:(FBLoginView *)loginView{
//    NSLog(@"Facebook Log in");
    //do not dismiss view here.
}
-(void) loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
//    NSLog(@"Facebook Log out");
    //self.username.text = @"You're not logged in";
    [self resetLoginView];
    
}
-(void) loginView:(FBLoginView *)loginView handleError:(NSError *)error{
    NSString *alertMessage, *alertTitle;
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
//        NSLog(@"user cancelled login");
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

-(void) dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)dissmissLoginView:(id)sender {
    [self dismissView];
}

@end
