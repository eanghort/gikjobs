//
//  GJWebConnection.m
//  gikjobs
//
//  Created by Eanghort Choeng on 17/11/2014.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJWebConnection.h"
#import "Constants.h"

@implementation GJWebConnection

+ (void)sendDataViaUrl:(NSString *)url WithData:(NSMutableDictionary *)data{
	
	NSString *myRequestString = [[NSString alloc] initWithFormat:@""];
	
	for (NSString* str in data) {
		
		NSString* str1 = [NSString stringWithFormat:@"%@=%@&", str, [data objectForKey:str]];
		myRequestString = [myRequestString stringByAppendingString:str1];
		
	}
	
	NSString* link = [[NSString alloc]initWithFormat:@"%@%@", BACKEND_BASE_URL, url];
	
	// Create Data from request
	NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: link]];
	
	// set Request Type
	[request setHTTPMethod: @"POST"];
	// Set content-type
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
	// Set Request Body
	[request setHTTPBody: myRequestData];
	// Now send a request and get Response
	NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
	// Log Response
	NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
	NSLog(@"%@",response);
}

@end
