//
//  VLRangeSlider.h
//  gikjobs
//
//  Created by Vanara Leng on 7/23/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJRangeSlider : UIControl
{
    float minValue;
    float maximumValue;
    float minimumRange;
    NSInteger selectedMinimumValue;
    NSInteger selectedMaximumValue;
    
    BOOL _maxThumbOn;
    BOOL _minThumbOn;
    
    float _padding;
    
    UIImageView * _minThumb;
    UIImageView * _maxThumb;
    UIView * _track;
    UIView * _trackBackground;
}

@property (nonatomic) float minimumValue;
@property (nonatomic) float maximumValue;
@property (nonatomic) float minimumRange;
@property (nonatomic) NSInteger selectedMinimumValue;
@property (nonatomic) NSInteger selectedMaximumValue;

- (id)initWithFrame:(CGRect)frame
           minValue:(float)paramMinValue
           maxValue:(float) paramMaxValue
           minRange:(float) paramMinRange
   selectedMinValue:(NSInteger)paramSelectedMinValue
   selectedMaxValue:(NSInteger)paramSelectedMaxValue;

@end
