//
//  Constants.h
//  gikjobs
//
//  Created by Vanara Leng on 8/27/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Constants <NSObject>

#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] integerValue]
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


//FEED
#define N0_FEED_WILL_LOAD 4

#define JOB_TYPES @[@"FT", @"PT", @"Tr"]


//COLOR

//Theme Color

#define THEME_COLOR UIColorFromRGB(0x009966 )
#define THEME_LIGHT_COLOR [UIColor lightGrayColor]
#define THEME_LIGHTER_COLOR [UIColor groupTableViewBackgroundColor]

#define WHITE_COLOR [UIColor whiteColor]

//page title
#define  RECOMMENDATION_PAGE_TITLE @"Recommend"
#define FEED_PAGE_TITLE @"Feeds"
#define PERSONAL_PAGE_TITLE @"Personal"


//storyboard
#define IPHONE_STORYBOARD_ID @"Main_iPhone"
#define IPAD_STORYBOARD_ID @"Main_iPad"

#define LOGIN_VIEW_CONTROLLER_ID @"LoginViewController"
#define FEED_VIEW_CONTROLLER_ID @"FeedViewController"
#define FEED_SETTING_VIEW_CONTROLLER_ID @"SearchSettingViewController"
#define PERSONAL_VIEW_CONTROLLER_ID @"PersonalViewController"
#define PAGE_VIEW_CONTROLLER_ID @"PageViewController"
#define PROFILE_SETTING_VIEW_CONTROLLER_ID @"ProfileViewController"
#define FEED_LIST_VIEW_CONTROLLER @"FeedListViewController"
#define ACADEMIC_SETTING_VIEW_CONTROLLER @"AcademicSettingViewController"
#define EXPERIENCE_SETTING_VIEW_CONTROLLER @"ExperienceSettingViewController"
#define FEED_DETAIL_VIEW_CONTROLLER @"FeedDetailViewController"

//3rd-Party
#define GOOGLE_CLIENT_ID @"179205440390-1mdb12gvs15ppo9eufl4jnlbn062jl6c.apps.googleusercontent.com"

//Menu
#define FAVOURITE_ITEM @"Favourite"
#define READING_LIST_ITEM @"Reading List"
#define PROFILE_SETTING_ITEM @"Profile Setting"
#define GENERAL_SETTING_ITEM @"General Setting"
#define SUPPORT_CENTER_ITEM @"Support Center"
#define LOG_OUT_ITEM @"Log Out"

//User
#define USER_PROFILE @"USRP"
#define USER_NAME @"USRN"
#define USER_EMAIL @"USRE"

#define BACKEND_BASE_URL @"http://localhost/"

@end
