//
//  VLProfileViewController.m
//  gikjobs
//
//  Created by Vanara Leng on 7/18/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJProfileSettingViewController.h"
#import "Constants.h"
#import <MobileCoreServices/MobileCoreServices.h> // Photo

#import "GJLanguageTableViewCell.h"
#import "GJExperienceTableViewCell.h"
#import "GJRangeSlider.h"
#import "GJAcademicTableViewCell.h"
#import "GJTagListView.h"
#import "GJImages.h"
#import "Constants.h"

#import "GJPickerViewController.h"
#import "GJAcademicSettingViewController.h"
#import "GJExperienceSettingViewController.h"

#define marginX 10.0f
#define marginY 10.0f
#define bodyFont [UIFont systemFontOfSize:17]

static NSString *languageCellIdentifier = @"LanguageCell";
static NSString *experienceCellIdentifier = @"ExperienceCell";
static NSString *academicCellIdentifier = @"AcademicCell";

@interface GJProfileSettingViewController ()


@property(nonatomic,strong)AFHTTPRequestOperationManager *manager;
//fetching property

@property (strong, nonatomic) NSMutableArray *languageList;
@property (strong, nonatomic) NSDictionary  *programmingList;
@property (strong, nonatomic) NSMutableArray *experienceList;
@property (strong, nonatomic) NSMutableArray *academicList;

//profile image
@property (strong, nonatomic) UIButton *profileImageView;
@property (strong, nonatomic) UIImagePickerController *imagePickerViewController;
@property (strong, nonatomic) UIActionSheet *actionSheet;

@property (strong, nonatomic) UITextField *userNameTextField;
@property (strong, nonatomic) UITextField *titleTextField;

@property (strong, nonatomic) UITextField *emailTextField;
@property (strong, nonatomic) UITextField *phoneTextField;
@property (strong, nonatomic) GJRangeSlider *salarySlider;
@property (strong, nonatomic) UITableView *languageTable;
@property (strong, nonatomic) GJTagListView *programmingButtonTagView;
@property (strong, nonatomic) UITableView *experienceTable;
@property (strong, nonatomic) UITableView *academicTable;
@property (strong, nonatomic) UITextView *prTextView;

@property (strong, nonatomic) UIView *emailBox;
@property (strong, nonatomic) UIView *phoneBox;
@property (strong, nonatomic) UIView *salaryBox;
@property (strong, nonatomic) UIView *languageBox;
@property (strong, nonatomic) UIView *programmingBox;
@property (strong, nonatomic) UIView *experienceBox;
@property (strong, nonatomic) UIView *academicBox;
@property (strong, nonatomic) UIView *prBox;

@property (strong, nonatomic) UIBarButtonItem *rightBarButtonItem;

@property (strong,nonatomic) UIButton *addLanugageButton;
@property (strong,nonatomic) UIButton *addProgrammingButton;
@property (strong,nonatomic) UIButton *addExperienceButton;
@property (strong,nonatomic) UIButton *addAcademicButton;

@property (strong,nonatomic) UITextField *activeTextField;
@property (strong,nonatomic) UITextView  *activeTextView;

@property CGPoint lastPoint;
@end

@implementation GJProfileSettingViewController

+(GJProfileSettingViewController*) shareProfileSetting{
    static GJProfileSettingViewController *profileSetting;
    
    static dispatch_once_t onceToken;
    if (!profileSetting) {
        dispatch_once(&onceToken, ^{
            
            profileSetting = [[UIStoryboard storyboardWithName:IPHONE_STORYBOARD_ID bundle:nil] instantiateViewControllerWithIdentifier:PROFILE_SETTING_VIEW_CONTROLLER_ID];
        });
    }
    return profileSetting;
}

#pragma mark - table
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.languageTable) {
        return [self.languageList count];
    }

    if (tableView == self.experienceTable) {
        return [self.experienceList count];
    }
    if (tableView == self.academicTable) {
        return [self.academicList count];
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.experienceTable) {
        return 123;
    }
    if (tableView == self.academicTable) {
        return 123;
    }
    if (tableView == self.languageTable){
        return 36;
    }
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.languageTable) {
        GJLanguageTableViewCell *langauge = [self.languageTable dequeueReusableCellWithIdentifier:languageCellIdentifier forIndexPath:indexPath];
        
        langauge.title.text = [[self.languageList objectAtIndex:indexPath.row] objectForKey:@"language"];
        langauge.selectionStyle = UITableViewCellSelectionStyleNone;
    
        return langauge;
    }
    
    if (tableView == self.experienceTable) {
        GJExperienceTableViewCell *experience = [self.experienceTable dequeueReusableCellWithIdentifier:experienceCellIdentifier forIndexPath:indexPath];
        experience.institute.text = [[self.experienceList objectAtIndex:indexPath.row] objectForKey:@"institute"];
        experience.department.text = [[self.experienceList objectAtIndex:indexPath.row] objectForKey:@"department"];
        experience.position.text= [[self.experienceList objectAtIndex:indexPath.row] objectForKey:@"position"];
        
        
        experience.fromYear.text= [[self.experienceList objectAtIndex:indexPath.row] objectForKey:@"from"];
        experience.toYear.text = [[self.experienceList objectAtIndex:indexPath.row] objectForKey:@"to"];
        
        experience.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        return experience;
    }
    if (tableView == self.academicTable) {
        GJAcademicTableViewCell *academic = [self.academicTable dequeueReusableCellWithIdentifier:academicCellIdentifier forIndexPath:indexPath];
        academic.university.text = [[self.academicList objectAtIndex:indexPath.row] objectForKey:@"university"];
        academic.department.text = [[self.academicList objectAtIndex:indexPath.row] objectForKey:@"department"];
        academic.graduationYear
        .text = [[self.academicList objectAtIndex:indexPath.row] objectForKey:@"graduateYear"];
        academic.specialization.text = [[self.academicList objectAtIndex:indexPath.row] objectForKey:@"specialization"];
        academic.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return academic;
    }
    return  nil;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.languageTable) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            [self.languageList removeObjectAtIndex:indexPath.row];
            
            [self.languageTable deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:(UITableViewRowAnimationBottom)];
            [self.languageTable reloadData];
            [self setFrame];
        }
    }
    if(tableView == self.experienceTable){
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            [self.experienceList removeObjectAtIndex:indexPath.row];

            [self.experienceTable deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationBottom];
            [self.experienceTable reloadData];
            [self setFrame];
        }
    }
    if(tableView == self.academicTable){
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            [self.academicList removeObjectAtIndex:indexPath.row];
            
            [self.academicTable deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationBottom];
            [self.academicTable reloadData];
            [self setFrame];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.academicTable) {

        // Edit Academic
        //set property after load view completion
        [self presentViewController:        [GJAcademicSettingViewController shareAcademicSetting] animated:YES completion:^{
            [GJAcademicSettingViewController shareAcademicSetting].university.text = ((GJAcademicTableViewCell*) [tableView cellForRowAtIndexPath:indexPath]).university.text;
            [GJAcademicSettingViewController shareAcademicSetting].department.text = ((GJAcademicTableViewCell*) [tableView cellForRowAtIndexPath:indexPath]).department.text;
            [GJAcademicSettingViewController shareAcademicSetting].specialization.text = ((GJAcademicTableViewCell*) [tableView cellForRowAtIndexPath:indexPath]).specialization.text;
            [GJAcademicSettingViewController shareAcademicSetting].graduationYear.text = ((GJAcademicTableViewCell*) [tableView cellForRowAtIndexPath:indexPath]).graduationYear.text;
        }];
    }
    if (tableView == self.experienceTable) {
        
        [self presentViewController:[GJExperienceSettingViewController shareExperienceSetting] animated:YES completion:^{
            [GJExperienceSettingViewController shareExperienceSetting].institute.text = ((GJExperienceTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]).institute.text;
            [GJExperienceSettingViewController shareExperienceSetting].department.text = ((GJExperienceTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]).department.text;
            [GJExperienceSettingViewController shareExperienceSetting].position.text = ((GJExperienceTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]).position.text;
            [GJExperienceSettingViewController shareExperienceSetting].fromYear.text = ((GJExperienceTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]).fromYear.text;
            [GJExperienceSettingViewController shareExperienceSetting].toYear.text = ((GJExperienceTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]).toYear.text;
            
        }];
    }
}

// enable table cell editing only tag edit mode is true
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.navigationItem.rightBarButtonItem.tag;
}

#pragma  mark - helper

-(CGFloat) contentHeightOfTable:(UITableView*) tableView{
    //return height of table
    [tableView layoutIfNeeded];
    return [tableView contentSize].height;
}
-(CGFloat) bottomOf:(UIView*) view{
    return view.frame.origin.y + view.frame.size.height;
}
- (UIView*) boxViewWithTitle:(NSString*)title{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.masksToBounds = NO;
    view.layer.cornerRadius = 4.0;
    view.layer.borderWidth=0.5;

    if (title==nil) {
        return view;
    }
    
    UILabel *label = [[UILabel alloc] init];
    [self labelStyleOf:label withText:title];
    [label setFont:[UIFont boldSystemFontOfSize:15]];
    label.frame = CGRectMake(10, 10, [self sizeOf:label].width, [self sizeOf:label].height);
    label.textColor =THEME_LIGHT_COLOR;
    [view addSubview:label];
    
    return  view;
}

- (UIButton*) addButtonWithAction:(SEL)selector{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(268,6, 26, 26)];
    [button setTitle:@"+" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
    button.layer.backgroundColor = THEME_COLOR.CGColor;
    button.layer.cornerRadius = 13;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(CGSize) sizeOf:(UIView*)view{
    //get view size.
    CGSize size = [view sizeThatFits:view.frame.size];
    return size;
}
-(CGFloat) rightOf:(UIView*) view{
    // get rightern x of view
    return (view.frame.size.width + view.frame.origin.x);
}
-(void) labelStyleOf:(UILabel*)label withText:(NSString*)text{
    label.textColor = [UIColor grayColor];
    label.text = text;
}



#pragma mark - init
-(void) initLanguageTable{

    self.languageTable = [[UITableView alloc] init];
    [self.languageTable setDelegate:self];
    [self.languageTable setDataSource:self];
    UINib *languageNib = [UINib nibWithNibName:@"GJLanguageTableViewCell" bundle:nil];
    [self.languageTable registerNib:languageNib forCellReuseIdentifier:languageCellIdentifier];
    

    //pre config
    [self.languageTable setBackgroundColor:[UIColor clearColor]];
    [self.languageTable setSeparatorStyle:(UITableViewCellSeparatorStyleSingleLineEtched)];
}
-(void) initExperienceTable{
    self.experienceTable= [[UITableView alloc] init];
    [self.experienceTable setDelegate:self];
    [self.experienceTable setDataSource:self];
    UINib *experienceNib = [UINib nibWithNibName:@"GJExperienceTableViewCell" bundle:nil];
    [self.experienceTable registerNib:experienceNib forCellReuseIdentifier:experienceCellIdentifier];

    //pre config
    [self.experienceTable setBackgroundColor:[UIColor clearColor]];
    
}
-(void) initAcademicTable{
    self.academicTable= [[UITableView alloc] init];
    UINib *academicNib = [UINib nibWithNibName:@"GJAcademicTableViewCell" bundle:nil];
    [self.academicTable registerNib:academicNib forCellReuseIdentifier:academicCellIdentifier];
    [self.academicTable setDelegate:self];
    [self.academicTable setDataSource:self];
    
    //pre config
    [self.academicTable setBackgroundColor:[UIColor clearColor]];
    [self.academicTable setSeparatorInset:UIEdgeInsetsZero];
    
}


// web service
- (void) fetchProfile
{
    [self.manager GET:[NSString stringWithFormat:@"http://somean.byethost4.com/gikjobs/profile.json"] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary  *responseObject){
        //on success
        [self setProperties:responseObject];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadTable];
            [self setFrame];
        });
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"error %@",error);
    }];
}

-(void) reloadTable{
    [self.languageTable reloadData];
    [self.experienceTable reloadData];
    [self.academicTable reloadData];
}

//set properties when fetch from web server
-(void) setProperties: (NSDictionary*)jsonObject{

    self.emailTextField.text = jsonObject[@"email"];
    self.phoneTextField.text = jsonObject[@"phone"];
    self.salarySlider.selectedMinimumValue = [jsonObject[@"minSalary"] integerValue];
    self.salarySlider.selectedMaximumValue= [jsonObject[@"maxSalary"] integerValue];
    self.languageList = [NSMutableArray arrayWithArray:jsonObject[@"languages"]] ;
    self.programmingList=jsonObject[@"programming"];

    
    self.academicList = [NSMutableArray arrayWithArray:jsonObject[@"academics"]];
    self.experienceList = [NSMutableArray arrayWithArray:jsonObject[@"experiences"]];
    self.prTextView.text = jsonObject[@"pr"];

    for ( NSDictionary*programming in self.programmingList) {
        [self.programmingButtonTagView addButtonWithTitle:programming[@"text"] enable:[programming[@"on"]  boolValue]  ID:[programming[@"id"] integerValue]];
    }
}

#pragma mark - Load UI
-(void) loadUI
{
    //call only once
    
    CGFloat width = self.scrollView.bounds.size.width;
    
    //Profile Image
    self.profileImageView = [[UIButton alloc] initWithFrame:CGRectMake((self.scrollView.bounds.size.width/2)-40,0 , 80, 80)];
    self.profileImageView.layer.cornerRadius = 40;
    self.profileImageView.clipsToBounds = YES;
    [[self.profileImageView imageView] setContentMode: UIViewContentModeScaleAspectFill];
    [self.profileImageView setImage:[UIImage imageNamed:@"profile-placeholder"] forState:UIControlStateNormal];
    
    [self.profileImageView addTarget:self action:@selector(performImageChange:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.profileImageView];
    
    
    //change photo button

    self.actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Select yours photo source."
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Camera", @"Photos", nil];
    
    //image picker view for profile photo
    self.imagePickerViewController = [[UIImagePickerController alloc] init];
    self.imagePickerViewController.delegate = self;
    
    //Username
    self.userNameTextField = [[UITextField alloc] initWithFrame:CGRectMake( 0, [self bottomOf:self.profileImageView], width,30)];
    self.userNameTextField.textAlignment = NSTextAlignmentCenter;
    [self.userNameTextField setFont:[UIFont boldSystemFontOfSize:17]];
    self.userNameTextField.placeholder = @"Full name";
    self.userNameTextField.textColor = THEME_COLOR;
    
    //title
    self.titleTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, [self bottomOf:self.userNameTextField], width,20)];
    self.titleTextField.textAlignment = NSTextAlignmentCenter;
    self.titleTextField.font = [UIFont systemFontOfSize:15];
    self.titleTextField.placeholder = @"Title";
    self.titleTextField.textColor = THEME_COLOR;
    
    //profile
    [self.scrollView addSubview:self.profileImageView];
    [self.scrollView addSubview:self.userNameTextField];
    [self.scrollView addSubview:self.titleTextField];
    
    //email
    UILabel *emailLabel = [[UILabel alloc] init];
    [self labelStyleOf:emailLabel withText:@"Email:"];
    emailLabel.frame = CGRectMake(marginX, marginY, [self sizeOf:emailLabel].width, [self sizeOf:emailLabel].height);
    emailLabel.textColor = THEME_LIGHT_COLOR;
    
    
    [emailLabel setFont:bodyFont];
    
    self.emailTextField = [[UITextField alloc] init];
    self.emailTextField.keyboardType= UIKeyboardTypeEmailAddress;
    [self.emailTextField setFont:bodyFont];
    self.emailTextField.placeholder = @"no email address";
    
    self.emailTextField.frame = CGRectMake([self rightOf:emailLabel]+10,marginY,width-[self rightOf:emailLabel]-40, [self sizeOf:emailLabel].height);
    [self.emailTextField setTextColor:THEME_COLOR];
    
    //email textfield event
    [self.emailTextField addTarget:self action: @selector(emailCompletion:) forControlEvents:UIControlEventEditingDidEnd];
    
    _emailBox = [self boxViewWithTitle:nil];
    
    
    [_emailBox addSubview:emailLabel];
    [_emailBox addSubview:self.emailTextField];
    [self.scrollView addSubview:_emailBox];
    
    //phone
    UILabel *phoneLabel = [[UILabel alloc] init];
    [self labelStyleOf:phoneLabel withText:@"Phone:"]; // set style to label
    phoneLabel.frame = CGRectMake(marginX, marginY, [self sizeOf:phoneLabel].width, [self sizeOf:phoneLabel].height);
    [phoneLabel setFont:bodyFont];
    phoneLabel.textColor = THEME_LIGHT_COLOR;
    
    
    self.phoneTextField = [[UITextField alloc] init];
    self.phoneTextField.keyboardType= UIKeyboardTypePhonePad;
    self.phoneTextField.placeholder = @"no phone number";
    [self.phoneTextField setFont:bodyFont];
    [self.phoneTextField setTextColor:THEME_COLOR];
    
    self.phoneTextField.frame = CGRectMake([self rightOf:phoneLabel]+4,marginY,width-[self rightOf:phoneLabel]-40, [self sizeOf:phoneLabel].height);
    self.phoneBox = [self boxViewWithTitle:nil];
    
    [self.phoneBox addSubview:phoneLabel];
    [self.phoneBox addSubview:self.phoneTextField];
    
    
    
    //phone textfield event
    [self.phoneTextField addTarget:self
                            action:@selector(phoneCompletion:) forControlEvents:UIControlEventEditingDidEnd];
    
    
    [self.scrollView addSubview:self.phoneBox];
    
    //prefered salary
    
    self.salaryBox=[self boxViewWithTitle:@"Salary Range"];
    
    self.salarySlider =[[GJRangeSlider alloc]initWithFrame:CGRectMake(marginX, marginY+30, width-30, 60)
                                                  minValue:100
                                                  maxValue:2000
                                                  minRange:200
                                          selectedMinValue:300
                                          selectedMaxValue:600];
    
    [self.salaryBox addSubview:self.salarySlider];
    [self.scrollView addSubview:self.salaryBox];
    
    //language
    
    self.languageBox = [self boxViewWithTitle:@"Language Skill"];
    [self.languageBox addSubview:self.languageTable];
    [self.scrollView addSubview:self.languageBox];
    
    //add language button
    self.addLanugageButton = [self addButtonWithAction:@selector(addLanguageAction:)];
    [self.languageBox addSubview:self.addLanugageButton];
    
    //programming language
    
    self.programmingBox = [self boxViewWithTitle:@"Programming Skill"];
    [self.programmingBox addSubview:self.programmingButtonTagView];
    [self.scrollView addSubview:self.programmingBox];
    
    //add experience button
    self.addProgrammingButton = [self addButtonWithAction:@selector(addProgrammingAction:)];
    [self.programmingBox addSubview:self.addProgrammingButton];
    
    //experience
    self.experienceBox = [self boxViewWithTitle:@"Experience"];
    [self.experienceBox addSubview:self.experienceTable];
    [self.scrollView addSubview:self.experienceBox];
    
    //add experience button
    self.addExperienceButton = [self addButtonWithAction:@selector(addExperienceAction:)];
    [self.experienceBox addSubview:self.addExperienceButton];
    
    //academic background
    
    self.academicBox = [self boxViewWithTitle:@"Academic Background"];
    [self.academicBox addSubview:self.academicTable];
    [self.scrollView addSubview:self.academicBox];
    
    //add academic button
    self.addAcademicButton = [self addButtonWithAction:@selector(addAcademicAction:)];
    [self.academicBox addSubview:self.addAcademicButton];
    
    
    //pr
    self.prTextView = [[UITextView alloc] initWithFrame:CGRectMake(marginX,31,width-40, 80)];
    [self.prTextView setFont:[UIFont systemFontOfSize:17]];
    self.prBox = [self boxViewWithTitle:@"PR"];
    [self.prBox addSubview:self.prTextView];
    [self.scrollView addSubview:self.prBox];
    self.prTextView.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.languageList = [[NSMutableArray alloc] init];
    
    //make it shown when modal display other view
    
    [self setModalPresentationStyle:UIModalPresentationCurrentContext];
    [self.navigationItem setTitle:PROFILE_SETTING_ITEM];
    
    //left button
    
    
    //right button
    self.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[GJImages EditImage] style:UIBarButtonItemStyleBordered target:self action:@selector(editProfile:)];
    
    [self.rightBarButtonItem setTag:0];
    [self.rightBarButtonItem setPossibleTitles:[NSSet setWithObjects:@"Edit",@"Done", nil]];
    self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
    
    /*Initialization Stage */
    
    [self initLanguageTable];
    [self initExperienceTable];
    [self initAcademicTable];
    self.programmingButtonTagView = [[GJTagListView alloc] initWithFrame:CGRectMake(marginX, 40, self.view.frame.size.width-40,30)];
    [self loadUI];
    
     self.manager = [AFHTTPRequestOperationManager manager];
    [self fetchProfile];
    
    //set Profile Picture
    if ([GJImages profilePictureWithRefresh:NO]) {
        [self.profileImageView setImage:[GJImages profilePictureWithRefresh:NO] forState:UIControlStateNormal];
    }

    [self activateEditing:NO];
    
    [self registerForKeyboardNotifications];
    
    UIGestureRecognizer *viewTouch = [[UIGestureRecognizer alloc] initWithTarget:self action:@selector(viewTouch:)];
    [self.scrollView addGestureRecognizer:viewTouch];
    
    
    
}

- (void) viewTouch:(id)sender{
    NSLog(@"view Touch");
}

- (void) viewWillAppear:(BOOL)animated{
    [self setFrame];
}

-(void) setFrame{
	CGFloat SPACE= 20.0f;

	CGFloat WIDTH=300.0f;
	CGFloat X=10.0f;
	CGFloat GAP=20.0f;
	
    self.emailBox.frame = CGRectMake(X,[self bottomOf:self.titleTextField]+10, WIDTH ,40 );
    self.phoneBox.frame = CGRectMake(X,[self bottomOf:self.emailBox]+4,WIDTH,40);
    self.salaryBox.frame = CGRectMake(X,[self bottomOf:self.phoneBox]+GAP, WIDTH, 100);
    
    //lanaguage
    self.languageBox.frame = CGRectMake(X,[self bottomOf:_salaryBox]+GAP, WIDTH, [self contentHeightOfTable:self.languageTable] + 40);
    self.languageTable.frame = CGRectMake(10, 31, WIDTH-40, [self contentHeightOfTable:self.languageTable]);
    
    //programming
    self.programmingBox.frame = CGRectMake(X,[self bottomOf:self.languageBox]+GAP, WIDTH,[self bottomOf:self.programmingButtonTagView] + 10);
    
    //experience
    self.experienceBox.frame = CGRectMake(X,[self bottomOf:_programmingBox]+GAP, WIDTH, [self contentHeightOfTable:self.experienceTable]+ 40);
    self.experienceTable.frame = CGRectMake(10, 31, WIDTH-30, [self contentHeightOfTable:self.experienceTable]);
    
    //academicf
    self.academicBox.frame = CGRectMake(X, [self bottomOf:self.experienceBox]+GAP, WIDTH, [self contentHeightOfTable:self.academicTable]+40);
    self.academicTable.frame = CGRectMake(10,31, WIDTH-30, [self contentHeightOfTable:self.academicTable]);
    
    self.prBox.frame = CGRectMake(X, [self bottomOf:self.academicBox]+GAP, WIDTH,[self bottomOf:self.prTextView]+10);
    
    self.scrollView.contentSize = CGSizeMake(WIDTH, [self bottomOf:self.prBox]+SPACE);
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self activateEditing:self.navigationItem.rightBarButtonItem.tag];
}
- (void) viewWillDisappear:(BOOL)animated{
    
}


#pragma mark - Image Picker

-(UIImage*)imageWithImage:(UIImage*)image
              scaledToWidth:(CGFloat)width;
{
    float oldWidth = image.size.width;
    float scaleFactor = width / oldWidth;
    
    float newHeight = image.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext( CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0,0,newWidth,newHeight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = (UIImage*) [info objectForKey:UIImagePickerControllerOriginalImage];
    image = [self imageWithImage:image scaledToWidth:250];
    
    NSData *pngData = UIImagePNGRepresentation(image);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"image.png"]; //Add the file name
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [pngData writeToFile:filePath atomically:YES]; //Write the file
        [self.profileImageView setImage:[GJImages profilePictureWithRefresh:YES] forState:UIControlStateNormal];
    }];
}


#pragma mark - text view
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.activeTextField = textField;
    
}
- (void) textFieldDidEndEditing:(UITextField *)textField{
    self.activeTextField = nil;
}

- (void) textViewDidBeginEditing:(UITextView *)textView{
    self.activeTextView = textView;
    NSLog(@"%@",self.activeTextView);
}
- (void) textViewDidEndEditing:(UITextView *)textView {
    self.activeTextView = nil;
    NSLog(@"%@",self.activeTextView);
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(70.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
    
    //for text field
    if (!CGRectContainsPoint(aRect, self.activeTextField.superview.frame.origin) ) {
        NSLog(@"active");
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextField.superview.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
    //for text view
    if (!CGRectContainsPoint(aRect, self.activeTextView.superview.frame.origin) ) {
        NSLog(@"active");
        CGPoint scrollPoint = CGPointMake(0.0, self.activeTextView.superview.frame.origin.y-kbSize.height+100);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(70, 0, 0, 0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - editing

// update

- (void) performImageChange:(id)sender{

    [self.actionSheet showInView:self.scrollView];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Camera"]) {
        self.imagePickerViewController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imagePickerViewController animated:YES completion:nil];
    }
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Photos"]) {
        self.imagePickerViewController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:self.imagePickerViewController animated:YES completion:nil];
    }
}

- (void) editProfile:(id)sender{
    if (self.rightBarButtonItem.tag == 0)
    {   //Edit
        self.rightBarButtonItem.tag = 1;
        [self.rightBarButtonItem setImage:[GJImages DoneImage]];
        [self activateEditing:YES];
    }
    else
    {   // Done
        self.rightBarButtonItem.tag = 0;
        [self.rightBarButtonItem setImage:[GJImages EditImage]];
        [self activateEditing:NO];
    }
}

- (void) activateEditing:(BOOL)editing{
    UIColor *borderColor = (editing)?THEME_COLOR:[UIColor lightGrayColor];
    UIColor *backgroundColor = (editing)? THEME_LIGHTER_COLOR  :[UIColor whiteColor];
    
    self.emailBox.layer.borderColor
    = self.phoneBox.layer.borderColor
    = self.salaryBox.layer.borderColor
    = self.languageBox.layer.borderColor
    = self.programmingBox.layer.borderColor
    = self.experienceBox.layer.borderColor
    = self.academicBox.layer.borderColor
    = self.prBox.layer.borderColor
    = borderColor.CGColor;
    
    
//    self.emailBox.layer.backgroundColor
//    = self.phoneBox.layer.backgroundColor
//    = self.salaryBox.layer.backgroundColor
//    = self.languageBox.layer.backgroundColor
//    = self.programmingBox.layer.backgroundColor
//    = self.experienceBox.layer.backgroundColor
//    = self.academicBox.layer.backgroundColor
//    = self.prBox.layer.backgroundColor
//    = backgroundColor.CGColor;
    
    [self.scrollView setBackgroundColor:backgroundColor];
    
    
    
    //hide add button
    [self.addLanugageButton setHidden:!editing];
    [self.addExperienceButton setHidden:!editing];
    [self.addAcademicButton setHidden:!editing];
    [self.addProgrammingButton setHidden:!editing];
    //disable view
    [self.emailTextField setEnabled:editing];
    [self.phoneTextField setEnabled:editing];
    
    [self.salaryBox setUserInteractionEnabled:editing];
    [self.programmingBox setUserInteractionEnabled:editing];
    [self.experienceTable setUserInteractionEnabled:editing];
    [self.academicTable setUserInteractionEnabled:editing];
    [self.prTextView setEditable:editing];
    
    [self.view endEditing:editing];
}

#pragma mark- action

    /* when use click (+) on Language Box */
- (void) addLanguageAction:(SEL) sender{
    [GJPickerViewController sharePickerView].url = @"http://somean.byethost4.com/gikjobs/languages.json";
    [GJPickerViewController sharePickerView].key = @"languages";
    
    [GJPickerViewController sharePickerView].selector = @selector(addLanguageCompletion:);
    
    [self presentViewController:[GJPickerViewController sharePickerView] animated:YES completion:nil];
}
    /* when use click (+) on Programming Skill Box */
- (void) addProgrammingAction:(SEL) sender{
    [GJPickerViewController sharePickerView].url = @"http://somean.byethost4.com/gikjobs/programming.json";
    [GJPickerViewController sharePickerView].key = @"programming";
    [GJPickerViewController sharePickerView].selector = @selector(addProgrammingCompletion:);
    [self presentViewController:[GJPickerViewController sharePickerView] animated:YES completion:nil];
}
    /* when use click (+) on Experience Box */
- (void) addExperienceAction:(SEL) sender{
    [GJExperienceSettingViewController shareExperienceSetting].selector = @selector(addExperienceCompletion:);
    
    [self presentViewController:[GJExperienceSettingViewController shareExperienceSetting] animated:YES completion:nil];
}
    /* when use click (+) on Academic Background Box */
- (void) addAcademicAction:(SEL) sender{
    [GJAcademicSettingViewController shareAcademicSetting].selector = @selector(addAcademicBackgroundCompletion:);
    
    [self presentViewController:[GJAcademicSettingViewController shareAcademicSetting] animated:YES completion:nil];
}

#pragma mark - completion
    /* Called in EndEditing on emailTextField */
- (void) emailCompletion:(id)sender{
    NSLog(@"Email : %@", self.emailTextField.text);
}
    /* Called in EndEditing on phoneTextField */
- (void)phoneCompletion:(id)sender{
    NSLog(@"Phone : %@",self.phoneTextField.text);
}

    /* Call in GJPickerViewController's Done*/
- (void) addLanguageCompletion:(id) sender{
    
    [GJPickerViewController sharePickerView].title = [GJPickerViewController sharePickerView].selectTitle;
    
    [self.languageList addObject:[NSDictionary dictionaryWithObject:[GJPickerViewController sharePickerView].title forKey:@"language"]];
    [self.languageTable reloadData];
    [self setFrame];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
    /* Call in GJPickerViewController's Done*/
- (void) addProgrammingCompletion:(id) sender{
    [self.programmingButtonTagView addButtonWithTitle:[GJPickerViewController sharePickerView].selectTitle enable:YES ID:[GJPickerViewController sharePickerView].selectID];
    [self dismissViewControllerAnimated:YES completion:nil];
}

    /*Called in GJExperienceSettingViewController's Done */
- (void)addExperienceCompletion:(id) sender{
    [self.experienceList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [GJExperienceSettingViewController shareExperienceSetting].institute.text, @"institute",
                                    [GJExperienceSettingViewController shareExperienceSetting].department.text, @"department",
                                    [GJExperienceSettingViewController shareExperienceSetting].position.text, @"position",
                                    [GJExperienceSettingViewController shareExperienceSetting].fromYear.text, @"from",
                                    [GJExperienceSettingViewController shareExperienceSetting].toYear.text, @"to"
                                    , nil]];
    [self.experienceTable reloadData];
    [self setFrame];
    [self dismissViewControllerAnimated:YES completion:nil];
}
    /*Called in GJPAcademicSettingViewController's Done */
- (void) addAcademicBackgroundCompletion:(id) sender{
    
    [self.academicList addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [GJAcademicSettingViewController shareAcademicSetting].department.text,@"department",
                                  [GJAcademicSettingViewController shareAcademicSetting].graduationYear.text, @"graduateYear",
                                  [GJAcademicSettingViewController shareAcademicSetting].specialization.text, @"specialization",
                                  [GJAcademicSettingViewController shareAcademicSetting].university.text, @"university",nil
                                  ]];
    [self.academicTable reloadData];
    [self setFrame];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end