//
//  VLLogInViewController.h
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//
// try not to use it more than one ( singleton )
#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>
@class GPPSignInButton;

@interface GJLogInViewController : UIViewController <GPPSignInDelegate,FBLoginViewDelegate>

@property(retain,nonatomic) GPPSignInButton *ggSignInView;
@property(retain,nonatomic)  FBLoginView *fbLoginView;
@property(retain,nonatomic) IBOutlet UIButton *ggSignOutView;
+(GJLogInViewController*) shareLoginView;

@end