//
//  VLSettingItemTableViewCell.h
//  Find IT Job
//
//  Created by Vanara Leng on 6/23/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJSettingItemTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UILabel *title;

@end