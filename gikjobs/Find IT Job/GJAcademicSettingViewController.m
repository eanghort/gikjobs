//
//  GJAddAcademicViewController.m
//  gikjobs
//
//  Created by Leng Vanara on 10/10/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJAcademicSettingViewController.h"
#import "GJProfileSettingViewController.h"
#import "Constants.h"
@interface GJAcademicSettingViewController () <UITextFieldDelegate>
@property (strong,nonatomic) UIScrollView *scrollView;

@end

@implementation GJAcademicSettingViewController
+(GJAcademicSettingViewController*) shareAcademicSetting{
    
    static GJAcademicSettingViewController *shareAcademicSetting;
    static dispatch_once_t onceToken;
    if (!shareAcademicSetting) {
        dispatch_once(&onceToken, ^{
            shareAcademicSetting = [[UIStoryboard storyboardWithName:IPHONE_STORYBOARD_ID bundle:nil] instantiateViewControllerWithIdentifier:ACADEMIC_SETTING_VIEW_CONTROLLER];
            [shareAcademicSetting setModalInPopover:YES];
            [shareAcademicSetting setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [shareAcademicSetting setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        });
    }
    
    return shareAcademicSetting;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView = [[UIScrollView alloc ] initWithFrame:self.view.frame];
    [self.scrollView addSubview:self.view];
    
    // Do any additional setup after loading the view.
    
    self.whiteView.layer.cornerRadius = 6.0;
    
    [self.doneButton addTarget:[GJProfileSettingViewController shareProfileSetting] action:self.selector forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *singleFingerTap =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.university.text = @"";
    self.department.text = @"";
    self.specialization.text = @"";
    self.graduationYear.text = @"";
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
