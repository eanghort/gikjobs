//
//  VLButtonTagView.h
//  gikjobs
//
//  Created by Vanara Leng on 7/25/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GJTag;
@interface GJTagListView : UIView

@property (strong,nonatomic) NSMutableArray *tagList;
@property CGPoint lastPoint;

- (void) addButtonWithTitle:(NSString*) title
                     enable:(BOOL)flag
                         ID:(NSInteger) ID;
-(CGFloat) heightOfTagList;
-(void) refreshFrame;
@end
