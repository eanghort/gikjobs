//
//  GJFeedListTableViewCell.h
//  gikjobs
//
//  Created by Leng Vanara on 10/8/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJFeedListTableViewCell : UITableViewCell
@property (nonatomic) int cellID;
@property (strong, nonatomic) IBOutlet UILabel *position;
@property (strong, nonatomic) IBOutlet UILabel *addDate;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@end