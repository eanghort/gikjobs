//
//  VLProfileLanguageTableViewCell.m
//  gikjobs
//
//  Created by Vanara Leng on 7/21/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJLanguageTableViewCell.h"
#import "Constants.h"
@implementation GJLanguageTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [self.title setTextColor: THEME_COLOR];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state

    
}

@end
