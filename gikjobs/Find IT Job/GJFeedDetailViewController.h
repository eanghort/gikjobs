//
//  VLFeedDetailViewController.h
//  gikjobs
//
//  Created by Vanara Leng on 7/21/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>
@interface GJFeedDetailViewController : UIViewController
@property NSInteger feedIndex;
+(GJFeedDetailViewController *) shareFeedDetailView;
@end
