//
//  VLFeedTableViewCell.m
//  Find IT Job
//
//  Created by Vanara Leng on 6/16/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJFeedTableViewCell.h"
#import "GJFeedViewController.h"
#import "Constants.h"
#import "GJImages.h"

#define BORDER_WIDTH .5

#define ACTIVE_COLOR UIColorFromRGB(0xC8C8C8 )
#define INACTIVE_COLOR [UIColor whiteColor]

#define DETAIL_BACKGROUND_COLOR UIColorFromRGB(0xFFFFFF)
#define FEED_BORDER_COLOR UIColorFromRGB(0xCCCCCC).CGColor

@implementation GJFeedTableViewCell

- (void)awakeFromNib
{
    // Initialization code

    [self setBackgroundColor: [UIColor clearColor]];
    
    [self.whiteRoundedCornerView setBackgroundColor:[UIColor whiteColor]];
    self.whiteRoundedCornerView.layer.masksToBounds = YES;
    self.whiteRoundedCornerView.layer.cornerRadius = 4.0;
	self.whiteRoundedCornerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
	self.whiteRoundedCornerView.layer.borderWidth = BORDER_WIDTH;

    
    self.likeButton.layer.borderColor = self.favouriteButton.layer.borderColor = self.downloadButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.likeButton.layer.borderWidth = self.favouriteButton.layer.borderWidth = self.downloadButton.layer.borderWidth = BORDER_WIDTH;
    
    self.clientProfileImageView.layer.cornerRadius = 2.0;
    self.clientProfileImageView.layer.masksToBounds = YES;    
    self.statusIndicator.layer.cornerRadius = 3.0;
}

#pragma mark - Buttons
//LIKE
- (IBAction)likeButtonAction:(id)sender {
    self.isLike = !self.isLike;
    [self setLikeButtonState:self.isLike];
}
- (void) setLikeButtonState:(BOOL)isLike{
    self.isLike = isLike;
    if (isLike){
        [self.likeButton setTitleColor: THEME_COLOR forState:UIControlStateNormal];
        [self.likeButton setImage:[GJImages LikeImageSelect] forState:UIControlStateNormal];
    }else{
        [self.likeButton setTitleColor: THEME_LIGHT_COLOR forState:UIControlStateNormal];
        [self.likeButton setImage:[GJImages LikeImage] forState:UIControlStateNormal];
    }
}
//FAVORITE
- (IBAction)favouriteButtonAction:(id)sender{
    self.isFavourite = !self.isFavourite;
    [self setFavoriteButtonState:self.isFavourite];
}
- (void) setFavoriteButtonState:(BOOL)isFavorite{
    self.isFavourite = isFavorite;
    if (isFavorite){
        [self.favouriteButton setTitleColor: THEME_COLOR forState:UIControlStateNormal];
        [self.favouriteButton setImage:[GJImages FavoriteImageSelect] forState:UIControlStateNormal];
    }else{
        [self.favouriteButton setTitleColor: THEME_LIGHT_COLOR forState:UIControlStateNormal];
        [self.favouriteButton setImage:[GJImages FavoriteImage] forState:UIControlStateNormal];
    }
}
//Download
- (IBAction)downloadButtonAction:(id)sender{
    self.isDownload = !self.isDownload;
    [self setDownloadButtonState:self.isDownload];
}

- (void)setDownloadButtonState:(BOOL) isDownload{
    self.isDownload = isDownload;
    if (isDownload){
        [self.downloadButton setImage:[GJImages DownloadImageSelect] forState:UIControlStateNormal];
        [self.downloadButton setTitleColor: THEME_COLOR forState:UIControlStateNormal];
    }else{
        [self.downloadButton setImage:[GJImages DownloadImage] forState:UIControlStateNormal];
        [self.downloadButton setTitleColor: THEME_LIGHT_COLOR forState:UIControlStateNormal];
    }
}

//read
-(void) setReadState:(BOOL)isRead{
    
    if (isRead) {
        [self.whiteRoundedCornerView setAlpha:1];
    }else{
        [self.whiteRoundedCornerView setAlpha:1];
    }
}
@end