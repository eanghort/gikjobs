//
//  VLFeedTableViewCell.h
//  Find IT Job
//
//  Created by Vanara Leng on 6/16/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJFeedTableViewCell : UITableViewCell

@property NSInteger cellIndex;

@property (strong, nonatomic) IBOutlet UIView *whiteRoundedCornerView;

@property (strong, nonatomic) IBOutlet UIImageView *clientProfileImageView;
@property (strong, nonatomic) IBOutlet UILabel *positionLabel;
@property (strong, nonatomic) IBOutlet UILabel *salaryLabel;
@property (strong, nonatomic) IBOutlet UILabel *deadlineLabel;

@property (strong, nonatomic) IBOutlet UILabel *noViewLabel;
@property (strong, nonatomic) IBOutlet UILabel *postTimeLabel;

@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *favouriteButton;
@property (strong, nonatomic) IBOutlet UIButton *downloadButton;
@property (strong, nonatomic) IBOutlet UILabel *jobTypeLabel;
@property (strong, nonatomic) IBOutlet UIView *statusIndicator;

@property BOOL isLike;
@property BOOL isFavourite;
@property BOOL isDownload;


- (IBAction)likeButtonAction:(id)sender;
- (IBAction)favouriteButtonAction:(id)sender;
- (IBAction)downloadButtonAction:(id)sender;

- (void) setLikeButtonState:(BOOL)isLike;
- (void) setFavoriteButtonState:(BOOL)isFavorite;
- (void) setDownloadButtonState:(BOOL) isDownload;
-(void) setReadState:(BOOL)isRead;
@end