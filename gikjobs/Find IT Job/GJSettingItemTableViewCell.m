//
//  VLSettingItemTableViewCell.m
//  Find IT Job
//
//  Created by Vanara Leng on 6/23/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJSettingItemTableViewCell.h"
#import "Constants.h"

@implementation GJSettingItemTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [self.title setTextColor:WHITE_COLOR];
    [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.1]];
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

@end
