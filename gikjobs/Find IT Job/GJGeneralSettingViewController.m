//
//  VLAppDelegate.h
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJGeneralSettingViewController.h"
#import "GJFeedViewController.h"
#import "GJRangeSlider.h"
#import "Constants.h"
#import "GJTagListView.h"
#import "GJTag.h"

@interface GJGeneralSettingViewController ()
@property (strong, nonatomic) GJRangeSlider *salaryRange;
@property (strong, nonatomic) NSMutableArray *programmingList;
@property (strong, nonatomic) IBOutlet UISegmentedControl *jobStatusSegment;
@property (strong, nonatomic) IBOutlet UISegmentedControl *jobTypeSegment;

@end

@implementation GJGeneralSettingViewController

+(GJGeneralSettingViewController*) shareSetting{
    static GJGeneralSettingViewController *shareSetting;

    static dispatch_once_t onceToken;
    if (!shareSetting) {
        dispatch_once(&onceToken, ^{
            
            shareSetting = [[UIStoryboard storyboardWithName:IPHONE_STORYBOARD_ID bundle:nil] instantiateViewControllerWithIdentifier:FEED_SETTING_VIEW_CONTROLLER_ID];
        });
    }
    return shareSetting;
}
- (IBAction)closeButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil ]];
    
    [self.jobStatusSegment setTintColor:THEME_COLOR];
    [self.jobTypeSegment setTintColor:THEME_COLOR];
    

    self.salaryRange = [[GJRangeSlider alloc]initWithFrame:CGRectMake(0, 220, self.view.bounds.size.width, 70)
                                                                           minValue:100
                                                                           maxValue:2000
                                                                           minRange:200
                                                                   selectedMinValue:300
                                                                   selectedMaxValue:600];
    [self.view addSubview:self.salaryRange];
    
    self.programmingList = [[NSMutableArray alloc] initWithObjects:@"C++",@"Java Script",@"Objective C", @"Python", @"PHP", nil];
    

    GJTagListView *programmingTagView = [[GJTagListView alloc] initWithFrame:CGRectMake(20, 400, self.view.bounds.size.width-40, 200)];
    
    for (int i = 0;  i< self.programmingList.count; i++) {
        [programmingTagView addButtonWithTitle:self.programmingList[i] enable:YES ID:i];
    }
    [self.view addSubview:programmingTagView];
    self.view.backgroundColor = [UIColor clearColor];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setTitle:@"General Setting"];
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
