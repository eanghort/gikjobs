//
//  VLProfileViewController.h
//  gikjobs
//
//  Created by Vanara Leng on 7/18/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>

@interface GJProfileSettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
+ (GJProfileSettingViewController*)shareProfileSetting;

@end
