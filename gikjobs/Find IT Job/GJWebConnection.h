//
//  GJWebConnection.h
//  gikjobs
//
//  Created by Eanghort Choeng on 17/11/2014.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GJWebConnection : NSObject

+ (void) sendDataViaUrl:(NSString*) url
			   WithData:(NSMutableDictionary*) postData;
@end
