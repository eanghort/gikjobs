//
//  VLFeedListTableViewController.h
//  gikjobs
//
//  Created by Leng Vanara on 10/8/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJFeedListTableViewController : UITableViewController
@property (strong,nonatomic) NSString *title;
@end
