//
//  VLAcademicBackgroundTableViewCell.m
//  gikjobs
//
//  Created by Vanara Leng on 7/24/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJAcademicTableViewCell.h"
#import "Constants.h"

@implementation GJAcademicTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    self.university.textColor
    = self.department.textColor
    = self.specialization.textColor
    = self.graduationYear.textColor
    = THEME_COLOR;
}

@end
