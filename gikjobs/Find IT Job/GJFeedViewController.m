//
//  VLAppDelegate.h
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJFeedViewController.h"
#import "GJFeedTableViewCell.h"
#import "GJFeedDetailViewController.h"
#import "Constants.h"
#import "GJFeedTableViewCell.h"

#define KEY_ID @"id"
#define KEY_LIKE @"like"
#define KEY_FAVORITE @"favourite"
#define KEY_DOWNLOAD @"download"

#define DEADLINE_COLOR @[FEED_OPEN_COLOR,FEED_CLOSE_COLOR,FEED_WARN_COLOR]

#define FEED_OPEN_COLOR UIColorFromRGB(0x009900)
#define FEED_CLOSE_COLOR [UIColor redColor]
#define FEED_WARN_COLOR [UIColor orangeColor]

static NSString *CellIdentifier = @"JobFeed";

@interface GJFeedViewController ()

@property(nonatomic,strong) NSMutableArray *feeds;
@property(nonatomic,assign) CGFloat lastScrollOffSet;
@property NSInteger numberOfRow;
@property (strong,nonatomic) UIView *activityIndicatorFooterView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation GJFeedViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self == [super initWithCoder:aDecoder]) {
        if([[[UIDevice currentDevice] systemVersion] integerValue]>=8.0){
            [self.navigationController setHidesBarsOnSwipe:YES];
        };
    }
    return self;
}

-(void) loadFromServer{
  
    
    [[AFHTTPRequestOperationManager manager] GET:[NSString stringWithFormat:@"http://somean.byethost4.com/gikjobs/feed.json"] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary  *responseObject){
        
        //on success
        [self.feeds addObjectsFromArray:responseObject[@"feeds"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"error %@",error);
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}


#pragma mark - scroll

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
    //if device version smaller than ios 8.0, use scroll view
    
    if(IOS_VERSION<8.0){
        if (scrollView.contentOffset.y<=-70) {
            if (self.navigationController.navigationBarHidden==YES) {
                [self.navigationController setNavigationBarHidden:NO animated:YES];
            }
        }else{
            if (self.navigationController.navigationBarHidden == NO) {
                [self.navigationController setNavigationBarHidden:YES animated:YES];
            }
        }
    };
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    if((self.tableView.contentSize.height - (scrollView.contentOffset.y + scrollView.bounds.size.height)) < 2000){
        self.tableView.tableFooterView = self.activityIndicatorFooterView;
        [(UIActivityIndicatorView *)[self.activityIndicatorFooterView viewWithTag:10] startAnimating];
        
        [self loadFromServer];
    }
}

-(void)initFooterView
{
    self.activityIndicatorFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.tableView.bounds.size.width, 40.0)];
    
    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    actInd.tag = 10;
    [actInd setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    actInd.frame = CGRectMake(150.0, 5.0, 20.0, 20.0);
    actInd.hidesWhenStopped = YES;
    [self.activityIndicatorFooterView addSubview:actInd];
    actInd = nil;
}

#pragma mark - View Protocol

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.feeds = [[NSMutableArray alloc] init];
    
    //set header space
    self.tableView.contentInset = UIEdgeInsetsMake(65, 0, 0, 0);
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(65, 0, 0, 0);
	
	
    [self initFooterView];
    
    //AFNetworking
    [self loadFromServer];
    
    //set delegate
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if([[[UIDevice currentDevice] systemVersion] integerValue]>=8.0){
        [self.navigationController setHidesBarsOnSwipe:YES];
    };

}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    if (self.view.tag == 0) {
//        [self.view setBackgroundColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.3]];
//    }else{
//        [self.view setBackgroundColor:[UIColor clearColor]];
//    }
    //hide on swipe 8.0 +
}

-(void)viewWillDisappear:(BOOL)animated{
    /* show navigation when view disappear */
    
    if([[[UIDevice currentDevice] systemVersion] integerValue]>=8.0){
        [self.navigationController setNavigationBarHidden:NO];
        [self.navigationController setHidesBarsOnSwipe:NO];
    };
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Feed Table

//table

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.feeds count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GJFeedTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self setFeedProperties:cell atIndex:indexPath];
        return cell;
}

#pragma mark - Set data

-(void) setFeedProperties:(GJFeedTableViewCell *)cell
                  atIndex:(NSIndexPath*) indexPath
{
    NSDictionary *feed = self.feeds[indexPath.row];
    
    cell.cellIndex = indexPath.row;
    
    cell.positionLabel .text = feed[@"position"];
    cell.salaryLabel.text = [NSString stringWithFormat:@"%@$ - %@$", feed[@"minSalary"],feed[@"maxSalary"]];
    cell.deadlineLabel.text = feed[@"deadline"];
    cell.noViewLabel.text = [NSString stringWithFormat:@"%@ views", feed[@"noView"]];
    
    if ([feed[@"noLike"] isEqualToString:@"0"]) {
        [cell.likeButton setTitle:@"Like" forState:UIControlStateNormal];
    }else{
        [cell.likeButton setTitle:feed[@"noLike"] forState:UIControlStateNormal];
    }

    //job type:
    cell.jobTypeLabel.text = JOB_TYPES[[feed[@"jobType"] integerValue]];

    //indicator
    [cell.statusIndicator setBackgroundColor: DEADLINE_COLOR[[feed[@"status"] integerValue]]];

    //button
    [cell setLikeButtonState:[feed[KEY_LIKE] boolValue]];
    [cell setFavoriteButtonState:[feed[KEY_FAVORITE] boolValue]];
    [cell setDownloadButtonState:[feed[KEY_DOWNLOAD] boolValue]];
    
    //read
    [cell setReadState:[feed[@"read"] boolValue]];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSDictionary *feed = self.feeds[indexPath.row];
    [GJFeedDetailViewController shareFeedDetailView].feedIndex = [feed[KEY_ID] integerValue];
    [self presentViewController:[GJFeedDetailViewController shareFeedDetailView] animated:YES completion:nil];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if (decelerate) {
		[self.searchBar resignFirstResponder];
	}
	
	if(scrollView.contentOffset.y < 0)
	{
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.2];
		
		self.tableView.contentInset = UIEdgeInsetsMake(65, 0, 0, 0);
		[self.searchBar becomeFirstResponder];
		
		[UIView commitAnimations];
	} else {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.2];
		
		self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
		[self.searchBar setText:@""];
		
		[UIView commitAnimations];
	}
}
 @end
