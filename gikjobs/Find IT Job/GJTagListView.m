//
//  VLButtonTagView.m
//  gikjobs
//
//  Created by Vanara Leng on 7/25/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJTagListView.h"
#import "GJTag.h"

@interface GJTagListView()
@property NSMutableArray *list;


@end
@implementation GJTagListView

@synthesize lastPoint;

- (CGSize) sizeOf:(UIView *) view{
    CGSize size = [view sizeThatFits:view.frame.size];
    return size;
}

-(CGFloat) rightOf:(UIView*) view{
    return (view.frame.size.width + view.frame.origin.x);
}

-(CGFloat) bottomOf:(UIView*) view{
    return view.frame.origin.y + view.frame.size.height;
}


//add
- (void) addButtonWithTitle:(NSString*) title
                     enable:(BOOL)flag
                         ID:(NSInteger) ID
{
    GJTag * tag = [[GJTag alloc] initWithTitle:title On:flag];
    
    [self.tagList addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:tag,@"tag",[NSNumber numberWithInteger:ID],@"id", nil]];
    
    [self refreshFrame];
}


//delete
- (void) removeButtonAtIndex:(NSInteger) indentity{
    [self.list removeObjectAtIndex:indentity];
}

- (void) refreshFrame{
    
    self.lastPoint = CGPointZero;
    
    CGFloat gap = 8.0;
    
    for (int i = 0; i<self.tagList.count; i++) {
        
        GJTag *tag = self.tagList[i][@"tag"];
        tag.frame = CGRectMake(0, 0, [self sizeOf:tag].width+10, 26);
        
        if (self.lastPoint.x == 0 && self.lastPoint.y == 0) {
            tag.frame = CGRectMake(0, 0, tag.frame.size.width,tag.frame.size.height);
            if (tag.frame.size.width > self.frame.size.width) {
                tag.frame = CGRectMake( 0, 0, self.frame.size.width, 26);
            }
        } else{
            tag.frame = CGRectMake(self.lastPoint.x, self.lastPoint.y, tag.frame.size.width, 26);
            if (tag.frame.size.width > (self.frame.size.width - self.lastPoint.x)) {
                tag.frame = CGRectMake(0,self.lastPoint.y + 30,tag.frame.size.width, 26);
            }
        }
        
        self.lastPoint = CGPointMake([self rightOf:tag]+gap, tag.frame.origin.y);
        [self addSubview:tag];
    }
    
//    for (GJTag *tag in self.tagList) {
//        tag.frame = CGRectMake(0, 0, [self sizeOf:tag].width+10, 26);
//        
//        if (self.lastPoint.x == 0 && self.lastPoint.y == 0) {
//            tag.frame = CGRectMake(0, 0, tag.frame.size.width,tag.frame.size.height);
//            if (tag.frame.size.width > self.frame.size.width) {
//                tag.frame = CGRectMake( 0, 0, self.frame.size.width, 26);
//            }
//        } else{
//            tag.frame = CGRectMake(self.lastPoint.x, self.lastPoint.y, tag.frame.size.width, 26);
//            if (tag.frame.size.width > (self.frame.size.width - self.lastPoint.x)) {
//                tag.frame = CGRectMake(0,self.lastPoint.y + 30,tag.frame.size.width, 26);
//            }
//        }
//        
//        self.lastPoint = CGPointMake([self rightOf:tag]+gap, tag.frame.origin.y);
//        [self addSubview:tag];
//    }
    //set frame after refresh
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.lastPoint.y + 26);
}

-(CGFloat) heightOfTagList{
    return self.lastPoint.y + 30;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.tagList = [[NSMutableArray alloc] init];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
