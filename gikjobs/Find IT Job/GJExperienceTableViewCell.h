//
//  VLProfileExperienceTableViewCell.h
//  gikjobs
//
//  Created by Vanara Leng on 7/24/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJExperienceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *institute;
@property (strong, nonatomic) IBOutlet UILabel *department;
@property (strong, nonatomic) IBOutlet UILabel *position;
@property (strong, nonatomic) IBOutlet UILabel *fromYear;
@property (strong, nonatomic) IBOutlet UILabel *toYear;

@end