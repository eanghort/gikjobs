//
//  VLFeedListTableViewController.m
//  gikjobs
//
//  Created by Leng Vanara on 10/8/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJFeedListTableViewController.h"
#import "GJFeedListTableViewCell.h"
#import "GJFeedDetailViewController.h"
#import <AFNetworking.h>
#import "Constants.h"

@interface GJFeedListTableViewController ()
@property (strong,nonatomic) NSMutableArray *list;
@end

@implementation GJFeedListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = YES;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.rightBarButtonItem = self.editButtonItem;

    [self.navigationItem setTitle:self.title];
    [self loadFromServer];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int ID = ((GJFeedListTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath]).cellID;
    [GJFeedDetailViewController shareFeedDetailView].feedIndex = ID;
    
    [self presentViewController:[GJFeedDetailViewController shareFeedDetailView] animated:YES completion:nil];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    GJFeedListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedListCell" forIndexPath:indexPath];
    // Configure the cell...
    cell.cellID = [self.list[indexPath.row][@"id"] intValue];
    cell.position.text = self.list[indexPath.row][@"position"];
    cell.addDate.text = self.list[indexPath.row][@"postOn"];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) loadFromServer{
    [[AFHTTPRequestOperationManager manager] GET:[NSString stringWithFormat:@"http://somean.byethost4.com/gikjobs/feed.json"] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary  *responseObject){
        
        //on success
        
        self.list = [NSMutableArray arrayWithArray:responseObject[@"feeds"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"error %@",error);
    }];
}

@end
