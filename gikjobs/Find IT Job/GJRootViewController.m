//
//  VLRootViewController.m
//  gikjobs
//
//  Created by Vanara Leng on 7/3/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJRootViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>
#import "GJAppDelegate.h"
#import "GJLogInViewController.h"

@interface GJRootViewController ()

@end

@implementation GJRootViewController

- (IBAction)goToSetting:(id)sender {
    
    [self presentViewController: [GJGeneralSettingViewController shareSetting] animated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	//set modal present style to navigation bar
    [self.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	[self.view setBackgroundColor:[UIColor clearColor]];
	
    //status bar
    UIView *statusBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [statusBar setBackgroundColor:THEME_COLOR];
    [statusBar setAlpha:0.95];
    
    [self.view addSubview:statusBar];
    
    //load all view controllers
    
    self.recommendationViewController = [self.storyboard instantiateViewControllerWithIdentifier:FEED_VIEW_CONTROLLER_ID];

    self.feedViewController = [self.storyboard instantiateViewControllerWithIdentifier:FEED_VIEW_CONTROLLER_ID];

    self.personalViewController = [self.storyboard instantiateViewControllerWithIdentifier:PERSONAL_VIEW_CONTROLLER_ID];
    
    self.recommendationViewController.title = RECOMMENDATION_PAGE_TITLE;
    self.feedViewController.title = FEED_PAGE_TITLE;
    self.personalViewController.title = PERSONAL_PAGE_TITLE;
    
    //Tag -> Index
    self.recommendationViewController.view.tag = 0;
    self.feedViewController.view.tag = 1;
    self.personalViewController.view.tag = 2;
    
    /* page view controller */
    //init and set protocol to PageViewController
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:PAGE_VIEW_CONTROLLER_ID];
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    
    //set view controller to pageViewController
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.pageViewController setViewControllers:@[self.feedViewController]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:nil
     ];

    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.view sendSubviewToBack:self.pageViewController.view];
    
    //navigation
    self.pageTitle.text = FEED_PAGE_TITLE;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
	
    //set modal present style
    
    //show login view if not logged in
    if (![FBSession activeSession].isOpen && ![[GPPSignIn sharedInstance] authentication]) {
        NSLog(@"not login");
       [self presentViewController:[GJLogInViewController shareLoginView] animated:YES completion:nil];
    }
}



#pragma mark - PageViewController

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = viewController.view.tag;
    
    if (index == 0 || index == NSNotFound) {
        return nil;
    }
    index--;
    
    
    return [self viewControllerAtIndex:index];
}
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = viewController.view.tag;
    
    if (index == 2 || index == NSNotFound) {
        return nil;
    }
    index++;
    
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController*) viewControllerAtIndex:(NSUInteger) index
{
    if (index==0) {
        return self.recommendationViewController;
    }
    if (index==1) {
        return self.feedViewController;
    }
    if (index==2) {
        return self.personalViewController;
    }
    return nil;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


#pragma mark - Page Controller

//Set page to page controller
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed{
        NSUInteger index = ((UIViewController*) [self.pageViewController.viewControllers lastObject]).view.tag;
    self.pageControl.currentPage = index;
    self.pageTitle.text = ((UIViewController*) [self.pageViewController.viewControllers lastObject]).title;
}

- (IBAction)pageControlValueChange:(id)sender {
    
    [self gotoViewControllerAtIndex:self.pageControl.currentPage];
    
}
-(void) gotoViewControllerAtIndex:(NSUInteger)index{

    //set navigation bar title
    self.pageTitle.text= [self viewControllerAtIndex:index].title;
    
//    if (self.currentIndex > index) {
    
        [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:index]] direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
//    }else{
//        [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:index]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
//    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
