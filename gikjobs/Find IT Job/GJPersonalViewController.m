//
//  VLAppDelegate.h
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

// note: method Facebook's Log in and Google's Log in call only once
#import "GJImages.h"
#import "GJPersonalViewController.h"
#import "GJSettingItemTableViewCell.h"
#import "GJProfileSettingViewController.h"
#import "Constants.h"
#import "GJLogInViewController.h"
#import "GJGeneralSettingViewController.h"
#import "GJFeedListTableViewController.h"
#import "GJFeedListTableViewCell.h"

static NSString *cellID = @"SettingItem";
@interface GJPersonalViewController () <UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong) NSArray*settingList;
@end

@implementation GJPersonalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.settingList.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GJSettingItemTableViewCell *cell = [self.profileCategoryTable dequeueReusableCellWithIdentifier:cellID];
    cell.title.text = [self.settingList objectAtIndex:indexPath.row];
    cell.icon.image = [UIImage imageNamed:[self.settingList objectAtIndex:indexPath.row]];
    
    return  cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
	[self setGradientBackground];
    
    self.profileCategoryTable.delegate = self;
    self.profileCategoryTable.dataSource = self;
    
    self.profileCategoryTable.contentInset = UIEdgeInsetsMake(70, 0, 0, 0);
    self.profileCategoryTable.scrollIndicatorInsets = UIEdgeInsetsMake(70, 0, 0, 0);
    
    self.ProfileImageView.layer.cornerRadius=40.0f;
    self.ProfileImageView.clipsToBounds=YES;                                      
    


    if (!self.settingList) {
        self.settingList = [NSArray arrayWithObjects:
                            FAVOURITE_ITEM,
                            READING_LIST_ITEM,
                            PROFILE_SETTING_ITEM,
                            GENERAL_SETTING_ITEM,
                            SUPPORT_CENTER_ITEM,
                            LOG_OUT_ITEM,
                            nil];
    }
}
-(void) setGradientBackground{
	// Create the colors
	UIColor *darkOp = THEME_LIGHTER_COLOR ;
	UIColor *lightOp = THEME_COLOR;
	// Create the gradient
	CAGradientLayer *gradient = [CAGradientLayer layer];
	
	// Set colors
	gradient.colors = [NSArray arrayWithObjects:
					   (id)darkOp.CGColor,
					   (id)darkOp.CGColor,
					   (id)lightOp.CGColor,
					   nil];
	
	// Set bounds
	gradient.frame = self.view.bounds;
	
	// Add the gradient to the view
	[self.view.layer insertSublayer:gradient atIndex:0];
}


- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
	
	//[self.fullNameLabel setText:<#(NSString *)#>];
    [self.ProfileImageView setImage:[GJImages profilePictureWithRefresh:NO]];
    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *title = self.settingList[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([title isEqualToString:FAVOURITE_ITEM]){
        GJFeedListTableViewController *feedListVC = [self.storyboard instantiateViewControllerWithIdentifier:FEED_LIST_VIEW_CONTROLLER];
        feedListVC.title = FAVOURITE_ITEM;
        [self.navigationController pushViewController:feedListVC animated:YES];
        
    }
    
    if ([title isEqualToString:READING_LIST_ITEM]){
        GJFeedListTableViewController *readingListVC = [self.storyboard instantiateViewControllerWithIdentifier:FEED_LIST_VIEW_CONTROLLER];
        readingListVC.title = READING_LIST_ITEM;
        [self.navigationController pushViewController:readingListVC animated:YES];
    }
    
    if ([title isEqualToString:LOG_OUT_ITEM]) {
        
        [self presentViewController: [GJLogInViewController shareLoginView] animated:YES completion:nil];
    }
    if ([title isEqualToString:PROFILE_SETTING_ITEM]) {
        
        [self.navigationController pushViewController:[GJProfileSettingViewController shareProfileSetting] animated:YES];
    }
    if ([title isEqualToString:GENERAL_SETTING_ITEM]) {
        
        [self.navigationController pushViewController:[GJGeneralSettingViewController shareSetting] animated:YES];
    }
}

@end