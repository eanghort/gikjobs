//
//  VLTag.m
//  gikjobs
//
//  Created by Vanara Leng on 7/25/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJTag.h"
#import "Constants.h"

@implementation GJTag

-(id) initWithTitle:(NSString *)title On:(BOOL)on{
    self=[super init];
    self.text = title;
//    [self setFont:[UIFont systemFontOfSize:17]];
    
    if (self) {
        self.on = !on;
        [self tagToggle:self];
        
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.layer.borderWidth=0.5;
        self.center = CGPointMake(frame.size.width/2, frame.size.height/2);
        
        self.layer.masksToBounds = YES;
 
        self.layer.cornerRadius = 6;
        [self setTextAlignment:NSTextAlignmentCenter];
        
        [self setUserInteractionEnabled:YES];
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tagToggle:)];
        [recognizer setNumberOfTapsRequired:1];
        [recognizer setNumberOfTouchesRequired:1];
        [self addGestureRecognizer:recognizer];
    }
    return self;
}

-(void) tagToggle: (id) sender{
    if (self.on == YES) {
        self.on = NO;
        self.textColor = THEME_LIGHT_COLOR;
        [self setBackgroundColor:WHITE_COLOR];
                self.layer.borderColor = THEME_LIGHT_COLOR.CGColor;
    }else{
        self.on = YES;
        self.textColor = WHITE_COLOR;
        [self setBackgroundColor:THEME_COLOR];
        self.layer.borderColor = THEME_COLOR .CGColor;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
