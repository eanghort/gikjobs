//
//  VLAppDelegate.h
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GJLogInViewController.h"
#import "GJGeneralSettingViewController.h"

@interface GJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong,nonatomic) UIWindow *window;
//@property (strong,nonatomic) GJLogInViewController *loginView;
@property (strong,nonatomic) GJGeneralSettingViewController *feedSettingViewController;
@end
