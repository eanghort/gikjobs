//
//  VLProfileLanguageTableViewCell.h
//  gikjobs
//
//  Created by Vanara Leng on 7/21/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJLanguageTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UILabel *title;

@end
