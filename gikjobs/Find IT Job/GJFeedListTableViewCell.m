//
//  GJFeedListTableViewCell.m
//  gikjobs
//
//  Created by Leng Vanara on 10/8/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJFeedListTableViewCell.h"
#import "Constants.h"
@implementation GJFeedListTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.position.textColor = THEME_COLOR;
    self.profileImage.layer.cornerRadius = 3.0;
    self.profileImage.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
