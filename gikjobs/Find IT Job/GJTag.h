//
//  VLTag.h
//  gikjobs
//
//  Created by Vanara Leng on 7/25/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJTag : UILabel
@property NSInteger programmingID;
@property BOOL on;
-(id) initWithTitle:(NSString*)title
                 On:(BOOL) on;
@end
