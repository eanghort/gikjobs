//
//  VLFeedDetailViewController.m
//  gikjobs
//
//  Created by Vanara Leng on 7/21/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJFeedDetailViewController.h"
#import "Constants.h"



#define bodyFont [UIFont systemFontOfSize:17]
 @interface GJFeedDetailViewController () <UIScrollViewDelegate>


@property(strong,nonatomic) NSURLSession *session;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) NSString *deadline;
@property (strong, nonatomic) NSString *salary;
@property (strong,nonatomic)  NSString *jobType;

@property (strong, nonatomic) NSString *position;
@property (strong, nonatomic) NSString *descriptions;

@property (strong, nonatomic) NSString *requirement;
@property (strong, nonatomic) NSString *howToApply;

@property (strong, nonatomic) NSString *contactEmail;
@property (strong, nonatomic) NSString *contactPhone;
@property (strong, nonatomic) NSString *remark;


@property (strong, nonatomic) UILabel *deadlineLabel;
@property (strong, nonatomic) UILabel *jobTypeLabel; //1 fulltime, 2 partime, 3 training
@property (strong, nonatomic) UILabel *salaryLabel;
@property (strong, nonatomic) UITextView *positionTextView;
@property (strong, nonatomic) UITextView *descriptionTextView;
@property (strong, nonatomic) UITextView *requirementTextView;
@property (strong, nonatomic) UITextView *howtoTextView;
@property (strong, nonatomic) UILabel *contactEmailLabel;
@property (strong, nonatomic) UILabel *contactPhoneLabel;
@property (strong, nonatomic) UITextView *remarkTextView;

@property (strong,nonatomic) UIView *overviewBox;
@property (strong,nonatomic) UIView *positionBox;
@property (strong,nonatomic) UIView *descriptionBox;
@property (strong,nonatomic) UIView *requirementBox;
@property (strong,nonatomic) UIView *howtoBox;
@property (strong,nonatomic) UIView *remarkBox;
@property (strong,nonatomic) UIView *contactBox;

@property (strong,nonatomic) UILabel *info;
@property (strong,nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;

@end

@implementation GJFeedDetailViewController

+(GJFeedDetailViewController *) shareFeedDetailView{
    static GJFeedDetailViewController *shareFeedDetailView;
    static dispatch_once_t onceToken;
    if (!shareFeedDetailView) {
        dispatch_once(&onceToken, ^{
			NSLog(@"%@",[UIDevice currentDevice].model);
			
			if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
				shareFeedDetailView = [[UIStoryboard storyboardWithName:IPHONE_STORYBOARD_ID bundle:nil] instantiateViewControllerWithIdentifier:FEED_DETAIL_VIEW_CONTROLLER];
				NSLog(@"Here");

				
			} else if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
				shareFeedDetailView = [[UIStoryboard storyboardWithName:IPAD_STORYBOARD_ID bundle:nil] instantiateViewControllerWithIdentifier:FEED_DETAIL_VIEW_CONTROLLER];
			}

            [shareFeedDetailView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        });
    }
    return shareFeedDetailView;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self == [super initWithCoder:aDecoder]) {
            [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self.scrollView setContentInset:UIEdgeInsetsMake(self.toolbar.frame.size.height, 0, 0, 0)];
	[self.scrollView setScrollIndicatorInsets:UIEdgeInsetsMake(self.toolbar.frame.size.height, 0, 0, 0)];
	[self.scrollView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
	[self.toolbar setBarTintColor:WHITE_COLOR];
	[self.toolbar.layer setShadowOffset:CGSizeMake(0, 0.5)];
	[self.toolbar.layer setShadowColor:THEME_LIGHT_COLOR.CGColor];
	[self.toolbar.layer setShadowOpacity:0.5];
	[self.toolbar.layer setShadowRadius:0.1];
	
	//close button
	UIBarButtonItem *closeButtonItem =((UIBarButtonItem*)self.toolbar.items[3]);
	
	closeButtonItem.tintColor = THEME_COLOR;
	closeButtonItem.target = self;
	closeButtonItem.action = @selector(dismissAction:);
	
	//add to favorite
	UIBarButtonItem *addToFavoriteButton = ((UIBarButtonItem*)self.toolbar.items[0]);
	addToFavoriteButton.tintColor = THEME_COLOR;
	addToFavoriteButton.target = self;
	addToFavoriteButton.action = @selector(addToFavoriteAction:);
	
	//add to readinglist
	UIBarButtonItem *addToReadingListButton = ((UIBarButtonItem*)self.toolbar.items[1]);
	addToReadingListButton.tintColor = THEME_COLOR;
	addToReadingListButton.target = self;
	addToReadingListButton.action = @selector(addToReadingListAction:);

	

	
	
    //set delegate of scroll view for using volecity.
    self.scrollView.delegate = self;
    [self initWebService];
    
    //Overiew
    self.overviewBox = [self boxViewWithTitle:@"Overview"];
    
    self.deadlineLabel = [self labelText:@"Dealine" labelValue:self.deadline];
    [self.overviewBox addSubview:self.deadlineLabel];
    
    self.salaryLabel = [self labelText:@"Salary" labelValue:self.salary];
    [self.overviewBox addSubview:self.salaryLabel];
    
    self.jobTypeLabel = [self labelText:@"Type" labelValue:self.jobType];
    [self.overviewBox addSubview:self.jobTypeLabel];
    
    [self.scrollView addSubview:self.overviewBox];
    
    //Position
    self.positionBox = [self boxViewWithTitle:@"Job Position"];
    self.positionTextView = [self textView];
    [self.positionBox addSubview:self.positionTextView];
    [self.scrollView addSubview:self.positionBox];
    
    //Description
    self.descriptionBox = [self boxViewWithTitle:@"Job Description"];
    self.descriptionTextView = [self textView];
    [self.descriptionBox addSubview:self.descriptionTextView];
    [self.scrollView addSubview:self.descriptionBox];
    
    //Requirement
    self.requirementBox = [self boxViewWithTitle:@"Job Requirement"];
    self.requirementTextView = [self textView];
    [self.requirementBox addSubview:self.requirementTextView];
    [self.scrollView addSubview:self.requirementBox];
    
    //Remark
    self.remarkBox = [self boxViewWithTitle:@"Remark"];
    self.remarkTextView = [self textView];
    [self.remarkBox addSubview:self.remarkTextView];
    [self.scrollView addSubview:self.remarkBox];
    
    //Contact
    
    self.contactBox = [self boxViewWithTitle:@"Contact"];
    self.contactEmailLabel = [self labelText:@"Email" labelValue:self.contactEmail];
    [self.contactBox addSubview:self.contactEmailLabel];
    
    self.contactPhoneLabel = [self labelText:@"Phone" labelValue:self.contactPhone];
    [self.contactBox addSubview:self.contactPhoneLabel];
    
    [self.scrollView addSubview:_contactBox];
    
    
    //Error message
    self.info = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.center.y-20, self.view.bounds.size.width, 26)];
    [self.info setTextAlignment:NSTextAlignmentCenter];
    [self.info setFont:bodyFont];
    [self.info setTextColor:THEME_COLOR];
    [self.info setText:@"Error loading. :-("];
    [self.info setHidden:YES];
    [self.scrollView addSubview:self.info];
    
    //set indicator
    self.indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.view.center.x-15, self.view.center.y-15,30, 30)];
    [self.indicator startAnimating];
    [self.indicator setColor:THEME_COLOR];
    [self.scrollView addSubview:self.indicator];
    
    
}

-(void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];

	
	[self setPropertyVisible:NO];
	[self.info setHidden:YES];
	[self.indicator setHidden:NO];
	[self fetchFeed];
	[self setFrame];
}
-(void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
		[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:(UIStatusBarAnimationNone)];
}
-(void) viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:(UIStatusBarAnimationFade)];
	
}
-(void) setUIData{
	self.deadlineLabel.text = [NSString stringWithFormat:@"Deadline : %@", self.deadline];
	self.salaryLabel.text =  [NSString stringWithFormat:@"Salary : %@",self.salary];
	self.jobTypeLabel.text = [NSString stringWithFormat:@"Type : %@", self.jobType];
	self.positionTextView.text = self.position;
	self.descriptionTextView.text = self.descriptions;
	self.requirementTextView.text = self.requirement;
	self.contactPhoneLabel.text = [NSString stringWithFormat:@"Phone: %@",self.contactPhone];
	self.contactEmailLabel.text = [NSString stringWithFormat:@"Email: %@",self.contactEmail];
	self.remarkTextView.text = self.remark;
}


// selectors
-(void) dismissAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) addToReadingListAction:(id)sender{
	NSLog(@"Add to reading list");
}
- (void) addToFavoriteAction:(id)sender{
	NSLog(@"Add to favorite");
}

- (UIView*) boxViewWithTitle:(NSString*)title{
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = WHITE_COLOR;
    view.layer.masksToBounds = NO;
    
    UILabel *label = [[UILabel alloc] init];
    [label setTextColor:THEME_COLOR];
    [label setFont:bodyFont];
    label.text = title;
    
    label.frame = CGRectMake(15, 5, [self sizeOf:label].width, [self sizeOf:label].height);
    
    [view addSubview:label];
    return  view;
}

-(CGSize) sizeOf:(UIView*)view{
    //get view size.
    CGSize size = [view sizeThatFits:view.frame.size];
    return size;
}

-(void) labelStyleOf:(UILabel*)label withText:(NSString*)text{
    label.textColor = [UIColor grayColor];
    label.text = text;
}


-(UILabel*) labelText:(NSString*)text
           labelValue:(NSString*)value;
{
    UILabel *label = [[UILabel alloc] init];

    label.text = [NSString stringWithFormat:@"%@ : %@",text,value ];
    [label setFont:bodyFont];
    
    return label;
    
}
-(UITextView*) textView{
    UITextView *textView = [[UITextView alloc] init];
	textView.font = bodyFont;
    textView.editable=NO;
    textView.scrollEnabled = NO;
    textView.textAlignment = NSTextAlignmentJustified;
    textView.backgroundColor = [UIColor clearColor];
    return textView;
}

-(void) initWebService{
    
    // Custom initialization of session for web service
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    [self fetchFeed];
}

-(void) fetchFeed{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"http://somean.byethost4.com/gikjobs/feedDetail%ld.json",(long)self.feedIndex] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary  *responseObject){
        
        //on success
        
        [self.indicator setHidden:YES];
        [self setProperties:responseObject];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setUIData];
            [self setFrame];
            [self setPropertyVisible:YES];
            
        });
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        [self.info setHidden:NO];
        [self.indicator setHidden:YES];
        [self.scrollView setContentSize:CGSizeZero];
    }];
}



-(void) setPropertyVisible:(BOOL)visible{
    [self.overviewBox setHidden:!visible];
    [self.positionBox setHidden:!visible];
    [self.descriptionBox setHidden:!visible];
    [self.requirementBox setHidden:!visible];
    [self.howtoBox setHidden:!visible];
    [self.remarkBox setHidden:!visible];
    [self.contactBox setHidden:!visible];
    
}



-(void) setProperties:(NSDictionary*) jsonObject{
    self.deadline = (jsonObject[@"deadline"])?jsonObject[@"deadline"]:@"No deadline";
    self.salary = (jsonObject[@"minSalary"]&&jsonObject[@"maxSalary"])?
    [NSString stringWithFormat:@"%@USD - %@USD",jsonObject[@"minSalary"] ,jsonObject[@"maxSalary"]]
    :@"Not limit";
    self.jobType = (jsonObject[@"jobType"])?jsonObject[@"jobType"]:@"Unknow";
    

    self.position = (jsonObject[@"position"])?(jsonObject[@"position"]):@"unknown position";
    self.descriptions= (jsonObject[@"description"])?jsonObject[@"description"]:@"no description";
    self.requirement= (jsonObject[@"requirement"])?jsonObject[@"requirement"]:@"no requirement";

    self.remark= (jsonObject[@"remark"])?jsonObject[@"remark"]:@"no remark";
    self.contactEmail= (jsonObject[@"email"])?jsonObject[@"email"]:@"no email";
    self.contactPhone = (jsonObject[@"phone"])?jsonObject[@"phone"]:@"no phone number";
    
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{

	[self setFrame];
	

}

-(void) setFrame{
	
	CGFloat PARA_WIDTH= [UIScreen mainScreen].applicationFrame.size.width - 30.0;
	CGFloat PARA_X = 10.0;
	CGFloat X=5.0;
	CGFloat WIDTH = [UIScreen mainScreen].applicationFrame.size.width - 10.0;
	CGFloat Y=26.0;
	CGFloat BOTTOM_HEIGHT=15.0;
	

	
    //scroll to top...
    [self.scrollView scrollRectToVisible:_overviewBox.frame animated:NO];

    self.overviewBox.frame =   CGRectMake(X, 0, WIDTH, 110);
    self.deadlineLabel.frame = CGRectMake(PARA_X+X, Y+10, PARA_WIDTH, 21);
    self.salaryLabel.frame =   CGRectMake(PARA_X+X, 2*Y+10, PARA_WIDTH, 21);
    self.jobTypeLabel.frame =  CGRectMake(PARA_X+X, 3*Y+10, PARA_WIDTH, 21);
    
    
    
    self.positionTextView.frame = CGRectMake(PARA_X, Y, PARA_WIDTH, [self.positionTextView sizeThatFits:CGSizeMake(PARA_WIDTH, 0)].height+BOTTOM_HEIGHT);
    self.positionBox.frame = CGRectMake(X, [self bottomOf:_overviewBox], WIDTH, self.positionTextView.bounds.size.height+BOTTOM_HEIGHT);
    
    
    self.descriptionTextView.frame = CGRectMake(PARA_X,Y,PARA_WIDTH, [self.descriptionTextView sizeThatFits:CGSizeMake(PARA_WIDTH, 0)].height+BOTTOM_HEIGHT);
    self.descriptionBox.frame = CGRectMake(X, [self bottomOf:_positionBox], WIDTH, self.descriptionTextView.bounds.size.height+BOTTOM_HEIGHT);
    
    self.requirementTextView.frame = CGRectMake(PARA_X,Y,PARA_WIDTH, [self.requirementTextView sizeThatFits:CGSizeMake(PARA_WIDTH, 0)].height+BOTTOM_HEIGHT);
    self.requirementBox.frame = CGRectMake(X, [self bottomOf:_descriptionBox], WIDTH,self.requirementTextView.bounds.size.height+BOTTOM_HEIGHT);

    
    self.requirementTextView.frame = CGRectMake(PARA_X,Y,PARA_WIDTH, [self.requirementTextView sizeThatFits:CGSizeMake(PARA_WIDTH, 0)].height+BOTTOM_HEIGHT);
    self.requirementBox.frame = CGRectMake(X, [self bottomOf:self.descriptionBox], WIDTH, _requirementTextView.bounds.size.height+BOTTOM_HEIGHT);
    
    self.remarkTextView.frame = CGRectMake(PARA_X,Y,PARA_WIDTH, [_remarkTextView sizeThatFits:CGSizeMake(PARA_WIDTH, 0)].height+BOTTOM_HEIGHT);
    self.remarkBox.frame = CGRectMake(X, [self bottomOf:_requirementBox], WIDTH, _remarkTextView.bounds.size.height+BOTTOM_HEIGHT);
    
    self.contactEmailLabel.frame = CGRectMake(PARA_X+X, Y, PARA_WIDTH, Y);
    self.contactPhoneLabel.frame = CGRectMake(PARA_X+X, Y*2 , PARA_WIDTH, Y);
    self.contactBox.frame = CGRectMake(X, [self bottomOf:_remarkBox], WIDTH, [self bottomOf:_contactPhoneLabel]+BOTTOM_HEIGHT);
    
    self.scrollView.contentSize = CGSizeMake([UIScreen mainScreen].applicationFrame.size.width,[self bottomOf:_contactBox]+BOTTOM_HEIGHT);

}



- (CGFloat) bottomOf:(UIView*)view{
    return view.frame.origin.y + view.frame.size.height+1;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//dismiss view when draggging.
-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
	if (velocity.y<=-2.0  /* || velocity.y >=2.0 */) {
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
