//
//  VLImages.m
//  gikjobs
//
//  Created by Vanara Leng on 9/8/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJImages.h"
#import "Constants.h"
@implementation GJImages

+ (UIImage *) image:(UIImage *)image withColor: (UIColor *) color
{
    CGRect contextRect;
    contextRect.origin.x = 0.0f;
    contextRect.origin.y = 0.0f;
    contextRect.size = [image size];
    // Retrieve source image and begin image context
    CGSize itemImageSize = [image size];
    CGPoint itemImagePosition;
    itemImagePosition.x = ceilf((contextRect.size.width - itemImageSize.width) / 2);
    itemImagePosition.y = ceilf((contextRect.size.height - itemImageSize.height) );
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions(contextRect.size, NO, [[UIScreen mainScreen] scale]); //Retina support
    else
        UIGraphicsBeginImageContext(contextRect.size);
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    // Setup shadow
    // Setup transparency layer and clip to mask
    CGContextBeginTransparencyLayer(c, NULL);
    CGContextScaleCTM(c, 1.0, -1.0);
    CGContextClipToMask(c, CGRectMake(itemImagePosition.x, -itemImagePosition.y, itemImageSize.width, -itemImageSize.height), [image CGImage]);
    
    // Fill and end the transparency layer
    color = [color colorWithAlphaComponent:1.0];
    
    CGContextSetFillColorWithColor(c, color.CGColor);
    
    contextRect.size.height = -contextRect.size.height;
    CGContextFillRect(c, contextRect);
    CGContextEndTransparencyLayer(c);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

+(UIImage *)LikeImageOrigin{
   static UIImage *image;
    if (!image) {
        image = [UIImage imageNamed:@"LikeButton"];
    }
    return image;
}

+ (UIImage *)LikeImage
{
    static UIImage *image;
    if (!image) {
        image = [self image:[self LikeImageOrigin] withColor:THEME_LIGHT_COLOR];
    }
    return image;
}


+ (UIImage *)LikeImageSelect
{
    static UIImage *image;
    if (!image) {
        image = [self image:[self LikeImage] withColor:THEME_COLOR];
    }
    return image;
}

+ (UIImage *)FavoriteImageOrigin{
    static UIImage *image;
    if (!image) {
        image = [UIImage imageNamed:@"FavoriteButton"];
        
        
    }
    return image;
}

+ (UIImage *)FavoriteImage{
    static UIImage *img;
    if (!img) {
        img = [self image:[self FavoriteImageOrigin] withColor:THEME_LIGHT_COLOR];
    }
    return img;
}

+ (UIImage *)FavoriteImageSelect{
    static UIImage *img;
    if (!img) {
        img = [self image:[self FavoriteImage] withColor:THEME_COLOR];
    }
    return img;
}

+ (UIImage *) DownloadImageOrigin{
    static UIImage *image;
    if (!image) {
        image = [UIImage imageNamed:@"DownloadButton"];
    }
    return image;
}

+ (UIImage *) DownloadImage{
    static UIImage *image;
    if (!image) {
        image = [self image:[self DownloadImageOrigin] withColor:THEME_LIGHT_COLOR];
    }
    return image;
}


+ (UIImage *) DownloadImageSelect{
    static UIImage *image;
    if (!image) {
        image = [self image:[self DownloadImage] withColor:THEME_COLOR];
    }
    return image;
}

// navigation
+ (UIImage *) DoneImage{
    static UIImage *image;
    if (!image) {
        image = [UIImage imageNamed:@"doneButton"];
    }
    return image;
}

+ (UIImage *) EditImage{
    static UIImage *image;
    if (!image) {
        image = [UIImage imageNamed:@"editButton"];
    }
    return image;
}

+ (NSData *) pngdata
{
    NSData *pngData;
    if (!pngData) {

    }
    
    return pngData;
}

+ (UIImage*) profilePictureWithRefresh:(BOOL) refresh
{
    static UIImage *image;
    if (!image || refresh) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        NSData *pngData = [NSData dataWithContentsOfFile:[documentsPath stringByAppendingPathComponent:@"image.png"]];
        image = [UIImage imageWithData:pngData];
    }
    return image;
}


@end
