//
//  VLAppDelegate.m
//  Find IT Job
//
//  Created by Vanara Leng on 5/6/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "GJAppDelegate.h"
#import "Constants.h"


@implementation GJAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //set background of app.
    [self.window setBackgroundColor:THEME_LIGHTER_COLOR];
    
    //show status bar after hide
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];

    //set Navigation Tint Color
    [[UINavigationBar appearance] setBarTintColor:THEME_COLOR];
    
    //init Login View
    [GJLogInViewController shareLoginView];
    
    return YES;
}

-(BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    BOOL FBHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    BOOL GGHandled = [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
    
    return GGHandled|FBHandled;
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    if ([FBSession activeSession].isOpen) {
//        NSLog(@"Facebook has been logged in");
        [[GJLogInViewController shareLoginView] dismissViewControllerAnimated:YES completion:nil];
        
    }
    if ([[GPPSignIn sharedInstance] authentication]) {
//        NSLog(@"Google has been logged in");
    }
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillResignActive:(UIApplication *)application{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
- (void)applicationDidEnterBackground:(UIApplication *)application{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}
- (void)applicationWillEnterForeground:(UIApplication *)application{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}
- (void)applicationWillTerminate:(UIApplication *)application{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end