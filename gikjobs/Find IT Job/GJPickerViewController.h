//
//  GJPickerViewController.h
//  gikjobs
//
//  Created by Vanara's Work on 9/11/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>
@interface GJPickerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *whiteView;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UIPickerView *PickerView;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *selectTitle;
@property (strong, nonatomic) NSString *url;
@property NSInteger selectID;
@property SEL selector;

+ (GJPickerViewController *) sharePickerView;

@end
