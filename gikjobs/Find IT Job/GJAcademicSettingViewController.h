//
//  GJAddAcademicViewController.h
//  gikjobs
//
//  Created by Leng Vanara on 10/10/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJAcademicSettingViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *whiteView;
@property (strong, nonatomic) IBOutlet UITextField *university;
@property (strong, nonatomic) IBOutlet UITextField *department;
@property (strong, nonatomic) IBOutlet UITextField *specialization;
@property (strong, nonatomic) IBOutlet UITextField *graduationYear;

@property (strong, nonatomic) IBOutlet UIButton *doneButton;

@property SEL selector;

+(GJAcademicSettingViewController*) shareAcademicSetting;

@end
