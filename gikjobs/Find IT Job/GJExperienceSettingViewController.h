//
//  GJExperienceSettingViewController.h
//  gikjobs
//
//  Created by Leng Vanara on 10/10/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJExperienceSettingViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *whiteView;
@property SEL selector;
@property (strong, nonatomic) IBOutlet UITextField *institute;
@property (strong, nonatomic) IBOutlet UITextField *department;
@property (strong, nonatomic) IBOutlet UITextField *position;
@property (strong, nonatomic) IBOutlet UITextField *fromYear;
@property (strong, nonatomic) IBOutlet UITextField *toYear;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

+(GJExperienceSettingViewController*) shareExperienceSetting;
@end
