//
//  VLAcademicBackgroundTableViewCell.h
//  gikjobs
//
//  Created by Vanara Leng on 7/24/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJAcademicTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *university;
@property (strong, nonatomic) IBOutlet UILabel *department;
@property (strong, nonatomic) IBOutlet UILabel *specialization;
@property (strong, nonatomic) IBOutlet UILabel *graduationYear;

@end
