//
//  VLProfileExperienceTableViewCell.m
//  gikjobs
//
//  Created by Vanara Leng on 7/24/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJExperienceTableViewCell.h"
#import "Constants.h"
@implementation GJExperienceTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    self.institute.textColor
    = self.department.textColor
    = self.position.textColor
    = self.fromYear.textColor
    = self.toYear.textColor
    = THEME_COLOR;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state

}

@end