//
//  GJExperienceSettingViewController.m
//  gikjobs
//
//  Created by Leng Vanara on 10/10/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import "GJExperienceSettingViewController.h"
#import "GJProfileSettingViewController.h"
#import "Constants.h"
@interface GJExperienceSettingViewController () <UITextFieldDelegate>

@end

@implementation GJExperienceSettingViewController
+ (GJExperienceSettingViewController *)shareExperienceSetting{
    static GJExperienceSettingViewController *shareExperienceSetting;
    static dispatch_once_t onceToken;
    if (!shareExperienceSetting) {
        dispatch_once(&onceToken, ^{
            
            shareExperienceSetting = [[UIStoryboard storyboardWithName:IPHONE_STORYBOARD_ID bundle:nil] instantiateViewControllerWithIdentifier:EXPERIENCE_SETTING_VIEW_CONTROLLER];
            
            //set modal present style
            [shareExperienceSetting setModalInPopover:YES];
            [shareExperienceSetting setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [shareExperienceSetting setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        });
    }
    
    return shareExperienceSetting;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.whiteView.layer.cornerRadius = 6.0;

    [self.doneButton addTarget:[GJProfileSettingViewController shareProfileSetting]
                        action:self.selector
              forControlEvents:UIControlEventTouchUpInside];

    UITapGestureRecognizer *singleFingerTap =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.institute.text = @"";
    self.department.text = @"";
    self.position.text = @"";
    self.fromYear.text = @"";
    self.toYear.text = @"";
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
