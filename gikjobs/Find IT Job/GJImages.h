//
//  VLImages.h
//  gikjobs
//
//  Created by Vanara Leng on 9/8/14.
//  Copyright (c) 2014 com.jcit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GJImages : NSObject




+ (UIImage*) LikeImage;
+ (UIImage*) LikeImageSelect;

+ (UIImage*) FavoriteImage;
+ (UIImage*) FavoriteImageSelect;

+ (UIImage*) DownloadImage;
+ (UIImage*) DownloadImageSelect;
+ (UIImage*) EditImage;
+ (UIImage*) DoneImage;
+ (UIImage*) profilePictureWithRefresh:(BOOL) refresh;

+ (UIImage *) image:(UIImage *)image withColor: (UIColor *) color;

@end
